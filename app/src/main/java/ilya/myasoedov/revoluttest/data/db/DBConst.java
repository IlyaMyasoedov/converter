package ilya.myasoedov.revoluttest.data.db;


public interface DBConst {
  String DATABASE_NAME = "revolut_ormlite.db";

  int DATABASE_VERSION = 1;

  String CURRENCY_TABLE_NAME = "currency";
}
