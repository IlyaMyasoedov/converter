package ilya.myasoedov.revoluttest.data.db;

import com.j256.ormlite.stmt.DeleteBuilder;
import ilya.myasoedov.revoluttest.data.db.dao.Dao;
import java.sql.SQLException;
import java.util.List;
import rx.Observable;

import static ilya.myasoedov.revoluttest.data.db.DatabaseRxUtils.toListRx;
import static ilya.myasoedov.revoluttest.data.db.DatabaseRxUtils.toRx;

public class Database<ID> {
  private final String TAG = getClass().getSimpleName();

  public static final Database<Long> INSTANCE = new Database<>();

  private SQLiteHelper sqLiteHelper;

  public Database() {
    sqLiteHelper = DatabaseHelperFactory.getHelper();
  }

  public <T> Observable<List<T>> listRx(Class<T> tClass) {
    return toListRx(getDao(tClass).list());
  }

  public <T> T get(ID id, Class<T> tClass) {
    return getDao(tClass).getById(id);
  }

  public <T> Observable<T> getByIdRx(ID id, Class<T> tClass) {
    return toRx(getDao(tClass).getById(id));
  }

  public <T> int removeById(ID id, Class<T> tClass) {
    return getDao(tClass).removeById(id);
  }

  public <T> Dao.CreateOrUpdateStatus createOrUpdate(T t, Class<T> tClass) {
    Dao<T, ID> dao = getDao(tClass);
    if (dao != null) return dao.createOrUpdate(t);
    return null;
  }

  public <T, ID> int deleteByParam(Class<T> tClass, String columnName, Object value) {
    try {
      DeleteBuilder deleteBuilder = getDao(tClass).deleteBuilder();
      deleteBuilder.where().eq(columnName, value);
      return deleteBuilder.delete();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return -1;
  }

  public <T> void clearTable(Class<T> tClass) {
    sqLiteHelper.getDaoFactory().clearTable(tClass);
  }

  public <T> Dao<T, ID> getDao(Class<T> tClass) {
    return sqLiteHelper.getDaoFactory().get(tClass);
  }

  SQLiteHelper getSqLiteHelper() {
    return sqLiteHelper;
  }
}
