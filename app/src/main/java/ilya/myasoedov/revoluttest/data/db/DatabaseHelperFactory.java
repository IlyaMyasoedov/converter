package ilya.myasoedov.revoluttest.data.db;

import android.content.Context;
import com.j256.ormlite.android.apptools.OpenHelperManager;

public class DatabaseHelperFactory {
  private static SQLiteHelper sqLiteHelper;

  public static SQLiteHelper getHelper(){
    return sqLiteHelper;
  }

  public static void setHelper(Context context){
    sqLiteHelper = OpenHelperManager.getHelper(context, SQLiteHelper.class);
  }

  public static void release(){
    OpenHelperManager.releaseHelper();
    sqLiteHelper = null;
  }
}
