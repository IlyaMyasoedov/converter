package ilya.myasoedov.revoluttest.data.db;

import java.util.List;
import rx.Observable;

public class DatabaseRxUtils {
  public static <T> Observable<T> toRx(T t) {
    return Observable.fromCallable(() -> t);
  }

  public static <T> Observable<List<T>> toListRx(List<T> t) {
    return Observable.fromCallable(() -> t);
  }
}
