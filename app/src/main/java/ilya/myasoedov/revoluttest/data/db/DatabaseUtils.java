package ilya.myasoedov.revoluttest.data.db;

import android.support.annotation.Nullable;
import com.j256.ormlite.misc.TransactionManager;
import ilya.myasoedov.revoluttest.data.db.dao.Dao;
import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import ilya.myasoedov.revoluttest.utils.CollectionUtils;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

public class DatabaseUtils {
  private DatabaseUtils() {
    throw new AssertionError();
  }

  public static <T> void saveLatest(final LatestLocal local, final Database<T> database) {
    try {
      TransactionManager.callInTransaction(database.getSqLiteHelper().getConnectionSource(),
          new Callable<Void>() {
            @Override public Void call() throws Exception {
              database.createOrUpdate(local, LatestLocal.class);
              return null;
            }
          });
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public static <T> void clear(final Database<T> database) {
    try {
      TransactionManager.callInTransaction(database.getSqLiteHelper().getConnectionSource(),
          new Callable<Void>() {
            @Override public Void call() throws Exception {
              database.clearTable(LatestLocal.class);
              return null;
            }
          });
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Nullable public static <T> LatestLocal getLatest(final Database<T> database) {
    Dao<LatestLocal, T> forecastItems = database.getDao(LatestLocal.class);
    List<LatestLocal> list = forecastItems.list();
    if (CollectionUtils.isEmpty(list)) {
      return null;
    } else {
      return forecastItems.list().get(0);
    }
  }
}
