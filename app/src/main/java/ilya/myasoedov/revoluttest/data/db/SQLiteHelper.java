package ilya.myasoedov.revoluttest.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import ilya.myasoedov.revoluttest.data.db.dao.DaoFactory;
import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import java.sql.SQLException;

import static ilya.myasoedov.revoluttest.data.db.DBConst.DATABASE_NAME;
import static ilya.myasoedov.revoluttest.data.db.DBConst.DATABASE_VERSION;

public class SQLiteHelper extends OrmLiteSqliteOpenHelper {
  private final String TAG = getClass().getSimpleName();

  private DaoFactory daoFactory;

  public SQLiteHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  private static final Class[] TABLE_CLASSES = {
      LatestLocal.class
  };

  @Override public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
    try {
      for (Class klass : TABLE_CLASSES) {
        TableUtils.createTable(connectionSource, klass);
      }
    } catch (SQLException e) {
      throw new RuntimeException("error creating DB " + DATABASE_NAME, e);
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i,
      int i2) {
    try {
      for (Class klass : TABLE_CLASSES) {
        TableUtils.dropTable(connectionSource, klass, true);
      }
      onCreate(sqLiteDatabase, connectionSource);
    } catch (SQLException e) {
      Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + i);
      throw new RuntimeException(e);
    }
  }

  DaoFactory getDaoFactory() {
    if (daoFactory == null) daoFactory = new DaoFactory(getConnectionSource());

    return daoFactory;
  }

  @Override public void close() {
    super.close();
  }
}
