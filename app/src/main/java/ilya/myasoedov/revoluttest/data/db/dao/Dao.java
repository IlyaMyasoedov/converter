package ilya.myasoedov.revoluttest.data.db.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Dao<T, ID> extends BaseDaoImpl<T, ID> implements IBaseDao<T, ID> {
  public Dao(ConnectionSource connectionSource, Class<T> dataClass) throws SQLException {
    super(connectionSource, dataClass);
  }

  @Override
  public List<T> list() {
    try {
      return queryForAll();
    } catch (SQLException e) {
      e.printStackTrace();
      return Collections.emptyList();
    }
  }

  @Override
  public List<T> listBySpecificField(String field, String value) {
    List<T> lists = null;
    try {
      lists = queryForEq(field, value);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return lists;
  }

  @Override
  public List<T> listBySpecificField(String field, List<String> values) {
    List<T> lists;
    try {
      Where<T, ID> where = queryBuilder().where();
      Iterator<String> iterator = values.iterator();
      if (iterator.hasNext()) {
        String value = iterator.next();
        where = where.eq(field, value);

        while (iterator.hasNext()) {
          value = iterator.next();
          where = where.or().eq(field, value);
        }
        lists = where.query();
      } else {
        lists = new ArrayList<>();
      }
    } catch (SQLException e) {
      lists = new ArrayList<>();
      e.printStackTrace();
    }
    return lists;
  }

  @Override
  public List<T> listBySpecificFieldNotEqual(String field, String value) {
    List<T> lists = null;
    try {
      lists = queryBuilder().where().ne(field, value).query();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return lists;
  }

  @Override
  public List<T> listBySpecificFieldNotEqual(String field, List<String> values) {
    List<T> lists;
    try {
      Where<T, ID> where = queryBuilder().where();
      Iterator<String> iterator = values.iterator();
      if (iterator.hasNext()) {
        String value = iterator.next();
        where = where.ne(field, value);

        while (iterator.hasNext()) {
          value = iterator.next();
          where = where.and().ne(field, value);
        }
        lists = where.query();
      } else {
        lists = new ArrayList<>();
      }
    } catch (SQLException e) {
      lists = new ArrayList<>();
      e.printStackTrace();
    }
    return lists;
  }

  @Override
  public T getById(ID id) {
    T t = null;
    try {
      t = queryForId(id);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return t;
  }

  @Override
  public T getBySpecificField(String field, String value) {
    T t = null;
    try {
      QueryBuilder<T, ID> queryBuilder = queryBuilder();
      queryBuilder.where().eq(field, value);
      PreparedQuery<T> preparedQuery = queryBuilder.prepare();
      t = queryForFirst(preparedQuery);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return t;
  }

  @Override
  public T getBySpecificFields(Map<String, String> valuesMap) {
    T t = null;
    try {
      QueryBuilder<T, ID> queryBuilder = queryBuilder();
      Where<T, ID> where = queryBuilder.where();
      Iterator<String> fieldsIterator = valuesMap.keySet().iterator();
      while (fieldsIterator.hasNext()) {
        String field = fieldsIterator.next();
        where.eq(field, valuesMap.get(field));
        if (fieldsIterator.hasNext()) {
          where.and();
        }
      }
      PreparedQuery<T> preparedQuery = queryBuilder.prepare();
      t = queryForFirst(preparedQuery);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return t;
  }

  @Override
  public int add(T t) {
    if (t == null) {
      return -1;
    }

    try {
      return create(t);
    } catch (SQLException sqle) {
      sqle.printStackTrace();
    }
    return -1;
  }

  @Override
  public int remove(T t) {
    if (t == null) {
      return -1;
    }

    int result = -1;
    try {
      result = delete(t);
    } catch (SQLException sqle) {
      sqle.printStackTrace();
    }
    return result;
  }

  @Override
  public int removeById(ID id) {
    int result = -1;
    try {
      result = deleteById(id);
    } catch (SQLException sqle) {
      sqle.printStackTrace();
    }
    return result;
  }

  @Override
  public int removeBySpecificField(String field, String value) {
    int result = -1;
    try {
      DeleteBuilder<T, ID> deleteBuilder = deleteBuilder();
      deleteBuilder.where().eq(field, value);
      result = deleteBuilder.delete();
    } catch (SQLException sqle) {
      sqle.printStackTrace();
    }
    return result;
  }

  @Override
  public int update(T t) {
    if (t == null) {
      return -1;
    }

    int result = -1;
    try {
      result = super.update(t);
    } catch (SQLException sqle) {
      sqle.printStackTrace();
    }
    return result;
  }

  @Override
  public CreateOrUpdateStatus createOrUpdate(T t) {
    if (t == null) return null;
    CreateOrUpdateStatus result = null;
    try {
      result = super.createOrUpdate(t);
    } catch (SQLException sqle) {
      sqle.printStackTrace();
    }
    return result;
  }
}
