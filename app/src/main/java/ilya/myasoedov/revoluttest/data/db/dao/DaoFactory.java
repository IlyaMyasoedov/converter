package ilya.myasoedov.revoluttest.data.db.dao;

import android.support.v4.util.SimpleArrayMap;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;

public class DaoFactory {
  private ConnectionSource connectionSource;

  private SimpleArrayMap<Class, Dao> daos = new SimpleArrayMap<>();

  public DaoFactory(ConnectionSource connectionSource) {
    this.connectionSource = connectionSource;
  }

  public <T, ID> Dao<T, ID> get(Class<T> tClass) {
    Dao<T, ID> dao = daos.get(tClass);
    if (dao == null) {
      try {
        dao = new Dao<>(connectionSource, tClass);
        daos.put(tClass, dao);
      } catch (SQLException sqle) {
        sqle.printStackTrace();
      }
    }
    return dao;
  }

  public <T> void clearTable(Class<T> tClass) {
    try {
      TableUtils.clearTable(connectionSource, tClass);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
