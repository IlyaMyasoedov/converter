package ilya.myasoedov.revoluttest.data.db.dao;

import com.j256.ormlite.dao.Dao;
import java.util.List;
import java.util.Map;

public interface IBaseDao<T, ID> {
  List<T> list();
  List<T> listBySpecificField(String field, String value);
  List<T> listBySpecificField(String field, List<String> values);
  List<T> listBySpecificFieldNotEqual(String field, String value);
  List<T> listBySpecificFieldNotEqual(String field, List<String> values);
  T getById(ID id);
  T getBySpecificField(String field, String value);
  T getBySpecificFields(Map<String, String> valuesMap);
  int add(T t);
  int removeById(ID id);
  int removeBySpecificField(String field, String value);
  int remove(T t);
  int update(T t);
  Dao.CreateOrUpdateStatus createOrUpdate(T t);
}
