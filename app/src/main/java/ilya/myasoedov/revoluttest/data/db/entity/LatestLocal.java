package ilya.myasoedov.revoluttest.data.db.entity;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ilya.myasoedov.revoluttest.data.db.DBConst;

@DatabaseTable(tableName = DBConst.CURRENCY_TABLE_NAME) public class LatestLocal
    implements Parcelable {
  @DatabaseField(id = true, useGetSet = true) private long id;

  @DatabaseField(useGetSet = true) private String base;

  @DatabaseField(useGetSet = true) private String date;

  @DatabaseField(useGetSet = true) private double gbp;

  @DatabaseField(useGetSet = true) private double eur;

  @DatabaseField(useGetSet = true) private double usd;

  public void setId(long id) {
    this.id = id;
  }

  public long getId() {
    return id;
  }

  public void setBase(String base) {
    this.base = base;
  }

  public String getBase() {
    return base;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getDate() {
    return date;
  }

  public void setGbp(double gbp) {
    this.gbp = gbp;
  }

  public double getGbp() {
    return gbp;
  }

  public void setEur(double eur) {
    this.eur = eur;
  }

  public double getEur() {
    return eur;
  }

  public void setUsd(double usd) {
    this.usd = usd;
  }

  public double getUsd() {
    return usd;
  }

  public LatestLocal() {
    id = 0;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(this.id);
    dest.writeString(this.base);
    dest.writeString(this.date);
    dest.writeDouble(this.gbp);
    dest.writeDouble(this.eur);
    dest.writeDouble(this.usd);
  }

  protected LatestLocal(Parcel in) {
    id = in.readLong();
    base = in.readString();
    date = in.readString();
    gbp = in.readDouble();
    eur = in.readDouble();
    usd = in.readDouble();
  }

  public static final Creator<LatestLocal> CREATOR = new Creator<LatestLocal>() {
    @Override public LatestLocal createFromParcel(Parcel source) {
      return new LatestLocal(source);
    }

    @Override public LatestLocal[] newArray(int size) {
      return new LatestLocal[size];
    }
  };
}
