package ilya.myasoedov.revoluttest.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * convenient class for data in CurrencyFragment
 */
public class CurrencyData implements Parcelable {
  private String base;
  private String currency;
  private double usd;
  private double gbp;
  private double eur;
  private boolean needToShowRate;

  public void setBase(String base) {
    this.base = base;
  }

  public String getBase() {
    return base;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getCurrency() {
    return currency;
  }

  public void setUsd(double usd) {
    this.usd = usd;
  }

  public double getUsd() {
    return usd;
  }

  public void setGbp(double gbp) {
    this.gbp = gbp;
  }

  public double getGbp() {
    return gbp;
  }

  public void setEur(double eur) {
    this.eur = eur;
    ;
  }

  public double getEur() {
    return eur;
  }

  public void setNeedToShowRate(boolean needToShowRate) {
    this.needToShowRate = needToShowRate;
  }

  public boolean getNeedToSHowRate() {
    return needToShowRate;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.base);
    dest.writeString(this.currency);
    dest.writeDouble(this.usd);
    dest.writeDouble(this.gbp);
    dest.writeDouble(this.eur);
    dest.writeInt(this.needToShowRate ? 1 : 0);
  }

  protected CurrencyData(Parcel in) {
    this.base = in.readString();
    this.currency = in.readString();
    this.usd = in.readDouble();
    this.gbp = in.readDouble();
    this.eur = in.readDouble();
    this.needToShowRate = in.readInt() == 1;
  }

  public CurrencyData() {

  }

  public static final Creator<CurrencyData> CREATOR = new Creator<CurrencyData>() {
    @Override public CurrencyData createFromParcel(Parcel source) {
      return new CurrencyData(source);
    }

    @Override public CurrencyData[] newArray(int size) {
      return new CurrencyData[size];
    }
  };

  public static CurrencyData copy(CurrencyData currencyData) {
    CurrencyData data = new CurrencyData();
    data.setBase(currencyData.getBase());
    data.setCurrency(currencyData.getCurrency());
    data.setUsd(currencyData.getUsd());
    data.setGbp(currencyData.getGbp());
    data.setEur(currencyData.getEur());
    data.setNeedToShowRate(currencyData.getNeedToSHowRate());
    return data;
  }
}
