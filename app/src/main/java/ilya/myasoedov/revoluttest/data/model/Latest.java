package ilya.myasoedov.revoluttest.data.model;

public class Latest {
  private String base;
  private String date;
  private Rates rates;

  public void setBase(String base) {
    this.base = base;
  }

  public String getBase() {
    return base;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getDate() {
    return date;
  }

  public void setRates(Rates rates) {
    this.rates = rates;
  }

  public Rates getRates() {
    return rates;
  }
}
