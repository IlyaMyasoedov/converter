package ilya.myasoedov.revoluttest.data.model;

import com.google.gson.annotations.SerializedName;

public class Rates {
  @SerializedName("USD") private double usd;
  @SerializedName("AUD") private double aud;
  @SerializedName("BGN") private double bgn;
  @SerializedName("BRL") private double brl;
  @SerializedName("CAD") private double cad;
  @SerializedName("CHF") private double chf;
  @SerializedName("CNY") private double cny;
  @SerializedName("CZK") private double czk;
  @SerializedName("DKK") private double dkk;
  @SerializedName("GBP") private double gbp;
  @SerializedName("HKD") private double hkd;
  @SerializedName("HRK") private double hrk;
  @SerializedName("HUF") private double huf;
  @SerializedName("IDR") private double idr;
  @SerializedName("ILS") private double ils;
  @SerializedName("INR") private double inr;
  @SerializedName("JPY") private double jpy;
  @SerializedName("KRW") private double krw;
  @SerializedName("MXN") private double mxn;
  @SerializedName("MYR") private double myr;
  @SerializedName("NOK") private double nok;
  @SerializedName("NZD") private double nzd;
  @SerializedName("PHP") private double php;
  @SerializedName("PLN") private double pln;
  @SerializedName("RON") private double ron;
  @SerializedName("RUB") private double rub;
  @SerializedName("SEK") private double sek;
  @SerializedName("SGD") private double sgd;
  @SerializedName("THB") private double thb;
  @SerializedName("TRY") private double trY;
  @SerializedName("ZAR") private double zar;
  @SerializedName("EUR") private double eur;

  public void setUsd(double usd) {
    this.usd = usd;
  }

  public double getUsd() {
    return usd;
  }

  public void setAud(double aud) {
    this.aud = aud;
  }

  public double getAud() {
    return aud;
  }

  public void setBgn(double bgn) {
    this.bgn = bgn;
  }

  public double getBgn() {
    return bgn;
  }

  public void setBrl(double brl) {
    this.brl = brl;
  }

  public double getBrl() {
    return brl;
  }

  public void setCad(double cad) {
    this.cad = cad;
  }

  public double getCad() {
    return cad;
  }

  public void setChf(double chf) {
    this.chf = chf;
  }

  public double getChf() {
    return chf;
  }

  public void setCny(double cny) {
    this.cny = cny;
  }

  public double getCny() {
    return cny;
  }

  public void setCzk(double czk) {
    this.czk = czk;
  }

  public double getCzk() {
    return czk;
  }

  public void setDkk(double dkk) {
    this.dkk = dkk;
  }

  public double getDkk() {
    return dkk;
  }

  public void setGbp(double gbp) {
    this.gbp = gbp;
  }

  public double getGbp() {
    return gbp;
  }

  public void setHkd(double hkd) {
    this.hkd = hkd;
  }

  public double getHkd() {
    return hkd;
  }

  public void setHrk(double hrk) {
    this.hrk = hrk;
  }

  public double getHrk() {
    return hrk;
  }

  public void setHuf(double huf) {
    this.huf = huf;
  }

  public double getHuf() {
    return huf;
  }

  public void setIdr(double idr) {
    this.idr = idr;
  }

  public double getIdr() {
    return idr;
  }

  public void setIls(double ils) {
    this.ils = ils;
  }

  public double getIls() {
    return ils;
  }

  public void setInr(double inr) {
    this.inr = inr;
  }

  public double getInr() {
    return inr;
  }

  public void setJpy(double jpy) {
    this.jpy = jpy;
  }

  public double getJpy() {
    return jpy;
  }

  public void setKrw(double krw) {
    this.krw = krw;
  }

  public double getKrw() {
    return krw;
  }

  public void setMxn(double mxn) {
    this.mxn = mxn;
  }

  public double getMxn() {
    return mxn;
  }

  public void setMyr(double myr) {
    this.myr = myr;
  }

  public double getMyr() {
    return myr;
  }

  public void setNok(double nok) {
    this.nok = nok;
  }

  public double getNok() {
    return nok;
  }

  public void setNzd(double nzd) {
    this.nzd = nzd;
  }

  public double getNzd() {
    return nzd;
  }

  public void setPhp(double php) {
    this.php = php;
  }

  public double getPhp() {
    return php;
  }

  public void setPln(double pln) {
    this.pln = pln;
  }

  public double getPln() {
    return pln;
  }

  public void setRon(double ron) {
    this.ron = ron;
  }

  public double getRon() {
    return ron;
  }

  public void setRub(double rub) {
    this.rub = rub;
  }

  public double getRub() {
    return rub;
  }

  public void setSek(double sek) {
    this.sek = sek;
  }

  public double getSek() {
    return sek;
  }

  public void setSgd(double sgd) {
    this.sgd = sgd;
  }

  public double getSgd() {
    return sgd;
  }

  public void setThb(double thb) {
    this.thb = thb;
  }

  public double getThb() {
    return thb;
  }

  public void setTrY(double trY) {
    this.trY = trY;
  }

  public double getTrY() {
    return trY;
  }

  public void setZar(double zar) {
    this.zar = zar;
  }

  public double getZar() {
    return zar;
  }

  public void setEur(double eur) {
    this.eur = eur;
  }

  public double getEur() {
    return eur;
  }
}
