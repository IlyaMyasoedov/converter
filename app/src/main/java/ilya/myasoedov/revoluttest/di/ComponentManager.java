package ilya.myasoedov.revoluttest.di;

import ilya.myasoedov.revoluttest.di.components.AppComponent;
import ilya.myasoedov.revoluttest.di.components.DaggerMainComponent;
import ilya.myasoedov.revoluttest.di.components.MainComponent;
import ilya.myasoedov.revoluttest.di.modules.main.MainModule;

public class ComponentManager {
  private AppComponent appComponent;
  private MainComponent mainComponent;

  public ComponentManager(AppComponent appComponent){
    this.appComponent = appComponent;
  }

  public MainComponent getMainComponent() {
    if (mainComponent == null) {
      mainComponent = DaggerMainComponent.builder()
          .appComponent(appComponent)
          .mainModule(new MainModule())
          .build();
    }

    return mainComponent;
  }
}
