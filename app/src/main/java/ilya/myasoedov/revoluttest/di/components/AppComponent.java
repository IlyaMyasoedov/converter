package ilya.myasoedov.revoluttest.di.components;

import android.content.Context;
import dagger.Component;
import ilya.myasoedov.revoluttest.di.modules.AppModule;
import ilya.myasoedov.revoluttest.di.scopes.ApplicationScope;

@ApplicationScope @Component(modules = { AppModule.class })
public interface AppComponent {
  Context context();
}
