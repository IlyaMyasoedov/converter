package ilya.myasoedov.revoluttest.di.components;

import dagger.Component;
import ilya.myasoedov.revoluttest.di.modules.main.MainModule;
import ilya.myasoedov.revoluttest.di.scopes.FragmentScope;
import ilya.myasoedov.revoluttest.ui.main.MainFragment;

@FragmentScope @Component(dependencies = AppComponent.class, modules = MainModule.class)
public interface MainComponent {
  void inject(MainFragment fragment);
}
