package ilya.myasoedov.revoluttest.di.modules;

import android.content.Context;
import dagger.Module;
import dagger.Provides;
import ilya.myasoedov.revoluttest.di.scopes.ApplicationScope;

@Module public class AppModule {
  private Context context;

  public AppModule(Context context) {
    this.context = context.getApplicationContext();
  }

  @Provides @ApplicationScope Context provideContext() {
    return context;
  }
}
