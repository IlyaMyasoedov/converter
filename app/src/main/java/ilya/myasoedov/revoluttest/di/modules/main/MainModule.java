package ilya.myasoedov.revoluttest.di.modules.main;

import android.content.Context;
import dagger.Module;
import dagger.Provides;
import ilya.myasoedov.revoluttest.di.scopes.FragmentScope;
import ilya.myasoedov.revoluttest.network.api.Api;
import ilya.myasoedov.revoluttest.ui.main.MainPresenter;
import ilya.myasoedov.revoluttest.ui.main.MainRepository;

@Module public class MainModule {

  @FragmentScope @Provides MainRepository provideMainRepository() {
    return new MainRepository(Api.get());
  }

  @FragmentScope @Provides MainPresenter provideMainPresenter(Context context,
      MainRepository repository) {
    return new MainPresenter(context, repository);
  }
}
