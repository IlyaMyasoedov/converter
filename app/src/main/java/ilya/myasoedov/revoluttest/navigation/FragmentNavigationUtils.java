package ilya.myasoedov.revoluttest.navigation;

import android.support.annotation.AnimRes;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import java.util.List;

public class FragmentNavigationUtils {
  public static void startFragment(FragmentManager fragmentManager, Fragment fragment,
      @IdRes int idFragment) {
    startFragment(fragmentManager, fragment, idFragment, 0, 0, 0, 0);
  }

  public static void startFragment(FragmentManager fragmentManager, Fragment fragment,
      @IdRes int idFragment, @AnimRes int enterAnim, @AnimRes int exitAnim,
      @AnimRes int popEnterAnim, @AnimRes int popExitAnim) {
    startFragment(fragmentManager, fragment, true, idFragment, enterAnim, exitAnim, popEnterAnim,
        popExitAnim);
  }

  public static void startFragment(FragmentManager fragmentManager, Fragment fragment,
      boolean hidePrevFragment, @IdRes int idFragment, @AnimRes int enterAnim,
      @AnimRes int exitAnim, @AnimRes int popEnterAnim, @AnimRes int popExitAnim) {
    startFragment(fragmentManager, fragment, hidePrevFragment, false, idFragment, enterAnim,
        exitAnim, popEnterAnim, popExitAnim);
  }

  public static void startFragment(FragmentManager fragmentManager, Fragment fragment,
      boolean hidePrevFragment, boolean replace, @IdRes int idFragment, @AnimRes int enterAnim,
      @AnimRes int exitAnim, @AnimRes int popEnterAnim, @AnimRes int popExitAnim) {
    if (fragment == null || fragmentManager == null) {
      return;
    }

    Fragment oldFragment = fragmentManager.findFragmentById(idFragment);
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    if (enterAnim > 0 && exitAnim > 0 && popEnterAnim > 0 && popExitAnim > 0) {
      fragmentTransaction.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim);
    }

    if (replace) {
      fragmentTransaction.replace(idFragment, fragment);
    } else {
      fragmentTransaction.add(idFragment, fragment);
    }
    fragmentTransaction.addToBackStack(fragment.getClass().getName());

    if (oldFragment != null) {
      oldFragment.onPause();
    }

    if (hidePrevFragment && oldFragment != null) {
      fragmentTransaction.hide(oldFragment);
    }

    fragmentTransaction.commitAllowingStateLoss();
  }

  public static void replaceFragment(FragmentManager fragmentManager, Fragment fragment,
      @IdRes int idFragment, @AnimRes int enterAnim, @AnimRes int exitAnim,
      @AnimRes int popEnterAnim, @AnimRes int popExitAnim) {
    startFragment(fragmentManager, fragment, false, true, idFragment, enterAnim, exitAnim,
        popEnterAnim, popExitAnim);
  }

  public static boolean back(FragmentManager fragmentManager) {
    if (fragmentManager == null) {
      return false;
    }

    int countFragment = fragmentManager.getBackStackEntryCount();
    if (countFragment > 1) {
      fragmentManager.popBackStack();
      return true;
    }

    return false;
  }

  public static void removeFragment(FragmentManager fragmentManager, Fragment fragment) {
    if (fragment == null || fragmentManager == null) {
      return;
    }

    fragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss();
  }

  public static Fragment getRootFragment(FragmentManager fragmentManager) {
    if (fragmentManager != null) {
      List<Fragment> fragments = fragmentManager.getFragments();
      if (fragments != null && fragmentManager.getBackStackEntryCount() > 0) {
        return fragments.get(0);
      }
    }

    return null;
  }

  public static Fragment getParentFragmentForFragment(FragmentManager fragmentManager,
      Fragment fragment) {
    if (fragmentManager != null) {
      List<Fragment> fragments = fragmentManager.getFragments();
      int parentIndex = -1;
      for (int index = 0; index < fragmentManager.getBackStackEntryCount(); index++) {
        Fragment frag = fragments.get(index);
        if (frag != null && frag.equals(fragment)) {
          parentIndex = index;
          break;
        }
      }
      if (parentIndex > 0) {
        return fragments.get(parentIndex - 1);
      } else if (parentIndex == 0) {
        return fragments.get(0);
      }
    }

    return null;
  }

  public static Fragment getTopFragment(FragmentManager fragmentManager) {
    if (fragmentManager != null) {
      List<Fragment> fragments = fragmentManager.getFragments();
      if (fragments != null
          && fragmentManager.getBackStackEntryCount() > 0
          && fragmentManager.getBackStackEntryCount() - 1 < fragments.size()) {
        return fragments.get(fragmentManager.getBackStackEntryCount() - 1);
      }
    }

    return null;
  }

  public static void clear(FragmentManager fragmentManager, String name, int flags) {
    if (fragmentManager == null) {
      return;
    }

    fragmentManager.popBackStackImmediate(name, flags);
  }
}
