package ilya.myasoedov.revoluttest.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.AnimRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.navigation.callback.NavigatorCallback;
import ilya.myasoedov.revoluttest.ui.main.MainFragment;

import static ilya.myasoedov.revoluttest.utils.ReflectionUtils.cast;

public class FragmentNavigatorImpl implements Navigator{
  private Activity activity;
  private FragmentManager fragmentManager;

  private NavigatorCallback callback;

  private AnimationHandler pushAnimationHandler;
  private AnimationHandler popAnimationHandler;

  public FragmentNavigatorImpl(Activity activity, FragmentManager fragmentManager) {
    this.activity = activity;
    this.fragmentManager = fragmentManager;

    pushAnimationHandler = getPushAnimation();
    popAnimationHandler = getPopAnimation();
  }

  @Override public void setNavigatorCallback(NavigatorCallback callback) {
    this.callback = callback;
  }

  @Override public void openMain() {
    open(new MainFragment());
  }

  @Override public void back() {
    if (callback != null) {
      callback.backCallback();
    }

    if (!FragmentNavigationUtils.back(fragmentManager)) {
      activity.finish();
    }
  }

  @Override public void setRoot(Object object) {
    setRoot(object, null);
  }

  @Override public void setRoot(Object object, Object args) {
    setRoot(object, null, 0);
  }

  @Override public void setRoot(Object object, Object args, int requestCode) {
    openForResult(object, args, requestCode);
  }

  @Override public void open(Object object) {
    open(object, null);
  }

  @Override public void open(Object object, Object args) {
    openForResult(object, args, 0);
  }

  @Override public void openForResult(Object object, int requestCode) {
    openForResult(object, null, requestCode);
  }

  @Override public void openForResult(Object object, Object args, int requestCode) {
    Fragment fragment = processBuildFragment(object, args, requestCode);
    if (fragment != null) {
      if (callback != null) {
        callback.openCallback();
      }

      FragmentNavigationUtils.startFragment(fragmentManager, fragment, R.id.container,
          pushAnimationHandler.enter, pushAnimationHandler.exit, popAnimationHandler.enter,
          popAnimationHandler.exit);
    }
  }

  @Override
  public void setResult(int requestCode, int resultCode, Intent data, Fragment currentFragment) {
    Fragment parentFragment =
        FragmentNavigationUtils.getParentFragmentForFragment(fragmentManager, currentFragment);
    if (parentFragment != null) {
      parentFragment.onActivityResult(requestCode, resultCode, data);
    }
  }

  private Fragment processBuildFragment(Object object, Object args, int requestCode) {
    if (object == null || !cast(Fragment.class, object)) {
      return null;
    }

    Fragment fragment = (Fragment) object;

    if (requestCode > 0) {
      Fragment targetFragment = null;
      if (fragmentManager.getBackStackEntryCount() > 0) {
        targetFragment =
            fragmentManager.getFragments().get(fragmentManager.getBackStackEntryCount() - 1);
      }

      fragment.setTargetFragment(targetFragment, requestCode);
    }

    if (args != null && cast(Bundle.class, args)) {
      fragment.setArguments((Bundle) args);
    }

    return fragment;
  }

  private AnimationHandler getPushAnimation() {
    return new AnimationHandler(android.R.anim.fade_in, android.R.anim.fade_out);
  }

  private AnimationHandler getPopAnimation() {
    return new AnimationHandler(android.R.anim.fade_in, android.R.anim.fade_out);
  }

  private static class AnimationHandler {
    private @AnimRes int enter;
    private @AnimRes int exit;

    public AnimationHandler(@AnimRes int enter, @AnimRes int exit) {
      this.enter = enter;
      this.exit = exit;
    }
  }
}
