package ilya.myasoedov.revoluttest.navigation;

import android.content.Intent;
import android.support.v4.app.Fragment;
import ilya.myasoedov.revoluttest.navigation.callback.NavigatorCallback;

public interface Navigator {
  void openMain();

  void back();

  void setNavigatorCallback(NavigatorCallback callback);

  void setRoot(Object object);

  void setRoot(Object object, Object args);

  void setRoot(Object object, Object args, int requestCode);

  void open(Object object);

  void open(Object object, Object args);

  void openForResult(Object object, int requestCode);

  void openForResult(Object object, Object args, int requestCode);

  void setResult(int requestCode, int resultCode, Intent data, Fragment currentFragment);
}
