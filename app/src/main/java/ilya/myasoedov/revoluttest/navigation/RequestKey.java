package ilya.myasoedov.revoluttest.navigation;

public interface RequestKey {
  String CURRENCY_DATA = "currency_data";
  String IS_BOTTOM = "is_bottom";
  String BASE_CURRENCY = "base_currency";
}
