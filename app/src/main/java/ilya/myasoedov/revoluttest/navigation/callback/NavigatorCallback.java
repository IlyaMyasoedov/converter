package ilya.myasoedov.revoluttest.navigation.callback;

public interface NavigatorCallback {
  void backCallback();

  void openCallback();
}
