package ilya.myasoedov.revoluttest.network.api;

import com.google.gson.Gson;
import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import ilya.myasoedov.revoluttest.data.net.LatestResponse;
import ilya.myasoedov.revoluttest.network.gson.GsonConverterFactory;
import ilya.myasoedov.revoluttest.network.utils.BaseAppConst;
import ilya.myasoedov.revoluttest.utils.ReconstructUtils;
import ilya.myasoedov.revoluttest.utils.RxUtils;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;

import static ilya.myasoedov.revoluttest.utils.CollectionUtils.mapOf;

public class Api {
  private static volatile Api sInstance;

  private final Retrofit retrofit;
  private final OkHttpClient client;

  private Gson gson;
  private GsonConverterFactory gsonConverter;

  //Impl
  private NetworkInterface.ICurrency forecastImpl;

  public static Api get() {
    if (sInstance == null) {
      synchronized (Api.class) {
        if (sInstance == null) {
          sInstance = new Api();
        }
      }
    }

    return sInstance;
  }

  private Api() {
    this(BaseAppConst.API_SERVER);
  }

  private Api(String apiServer) {
    this.client = ApiUtils.createHttpClient();
    this.gson = ApiUtils.defaultGson();
    this.gsonConverter = ApiUtils.defaultGsonConverter(gson);
    this.retrofit = ApiUtils.createRetrofit(apiServer, gsonConverter, client);
  }

  private NetworkInterface.ICurrency currency() {
    if (forecastImpl == null) {
      forecastImpl = retrofit.create(NetworkInterface.ICurrency.class);
    }
    return forecastImpl;
  }

  public Observable<LatestLocal> getLatest(String currency) {
    return currency().getLatest(mapOf("base", currency))
        .compose(RxUtils.applySchedulers())
        .flatMap(
            (LatestResponse response) -> Observable.just(ReconstructUtils.toLatestLocal(response)));
  }

}
