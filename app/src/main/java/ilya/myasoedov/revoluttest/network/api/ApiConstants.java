package ilya.myasoedov.revoluttest.network.api;

public interface ApiConstants {
  String PREFIX = "/";

  String LATEST = PREFIX + "latest";
}
