package ilya.myasoedov.revoluttest.network.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ilya.myasoedov.revoluttest.network.gson.GsonConverterFactory;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * helper class for api
 */

class ApiUtils {
  private static final int TIMEOUT = 15;
  private static final int WRITE_TIMEOUT = 20;
  private static final int CONNECT_TIMEOUT = 20;

  public static Gson defaultGson() {
    return new GsonBuilder().setLenient().create();
  }

  public static GsonConverterFactory defaultGsonConverter(Gson gson) {
    return GsonConverterFactory.create(gson);
  }

  public static OkHttpClient createHttpClient() {
    return new OkHttpClient.Builder().connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        .build();
  }

  public static Retrofit createRetrofit(String apiServer, GsonConverterFactory converter,
      OkHttpClient client) {
    return new Retrofit.Builder().baseUrl(apiServer)
        .addConverterFactory(converter)
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .client(client)
        .build();
  }
}
