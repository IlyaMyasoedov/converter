package ilya.myasoedov.revoluttest.network.api;

import ilya.myasoedov.revoluttest.data.net.LatestResponse;
import java.util.Map;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

import static ilya.myasoedov.revoluttest.network.api.ApiConstants.LATEST;

interface NetworkInterface {

  interface ICurrency {
    @GET(LATEST) Observable<LatestResponse> getLatest(@QueryMap Map<String, String> params);
  }
}
