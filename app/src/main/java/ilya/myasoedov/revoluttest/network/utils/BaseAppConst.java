package ilya.myasoedov.revoluttest.network.utils;

public class BaseAppConst {
  public static final String API_SERVER;

  private static final ServerSchema SERVER_SCHEMA = ServerSchema.HTTPS;

  static {
    API_SERVER = SERVER_SCHEMA.getServerSchema() + "://"+ "api.fixer.io";
  }
}
