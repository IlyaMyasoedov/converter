package ilya.myasoedov.revoluttest.network.utils;

public enum ServerSchema {
  HTTP("http"),
  HTTPS("https");

  private final String serverSchema;

  ServerSchema(String serverSchema){
    this.serverSchema = serverSchema;
  }

  public String getServerSchema() {
    return serverSchema;
  }
}
