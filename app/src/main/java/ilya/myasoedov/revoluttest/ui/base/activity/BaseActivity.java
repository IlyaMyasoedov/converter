package ilya.myasoedov.revoluttest.ui.base.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import butterknife.ButterKnife;
import ilya.myasoedov.revoluttest.navigation.Navigator;
import ilya.myasoedov.revoluttest.ui.base.fragment.BaseFragment;
import ilya.myasoedov.revoluttest.ui.base.fragment.OnBackPressedListener;
import ilya.myasoedov.revoluttest.utils.ArrayUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ilya.myasoedov.revoluttest.navigation.RequestCode.WITH_PERMISSIONS_REQUEST_CODE;
import static ilya.myasoedov.revoluttest.utils.AppsUtils.IS_MARSHMALLOW;

public abstract class BaseActivity extends AppCompatActivity {
  final String TAG = getClass().getSimpleName();

  private Navigator navigator;

  private String[] waitingPermissions;
  private Runnable codeWaitingForPermissions;


  @Override protected void onCreate(@Nullable Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(getLayoutId());
    ButterKnife.bind(this);

    navigator = createNavigator();
  }

  @Override public void onBackPressed() {
    FragmentManager fragmentManager = getSupportFragmentManager();
    List<Fragment> fragments = fragmentManager.getFragments();
    if (fragments != null && !fragments.isEmpty()) {
      Fragment fragment = fragments.get(0);
      if (fragment instanceof OnBackPressedListener) {
        ((OnBackPressedListener) fragment).onBackPressed();
      }
    }
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    FragmentManager fragmentManager = getSupportFragmentManager();
    List<Fragment> fragments = fragmentManager.getFragments();
    if (fragments != null) {
      for (int i = 0; i < fragments.size(); i++) {
        if (fragments.get(i) instanceof BaseFragment) {
          fragments.get(i).onActivityResult(requestCode, resultCode, data);
        }
      }
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    // runWithPermissions()
    if (runWithPermissions_onRequestPermissionResult(requestCode, permissions, grantResults)) {
      return;
    }

    // dispatch
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    FragmentManager fragmentManager = getSupportFragmentManager();
    List<Fragment> fragments = fragmentManager.getFragments();
    if (fragments != null) {
      for (int i = 0; i < fragments.size(); i++) {
        if (fragments.get(i) != null) {
          fragments.get(i).onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
      }
    }
  }

  @SuppressWarnings("NewApi") public void runWithPermissions(String[] permissions, Runnable code) {
    if (!IS_MARSHMALLOW) {
      code.run();
      return;
    }

    List<String> ungranted = new ArrayList<>(permissions.length);
    for (String permission : permissions) {
      if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
        ungranted.add(permission);
      }
    }
    if (ungranted.isEmpty()) {
      code.run();
      return;
    }

    requestPermissions(permissions, WITH_PERMISSIONS_REQUEST_CODE);
    waitingPermissions = permissions;
    codeWaitingForPermissions = code;
  }

  private boolean runWithPermissions_onRequestPermissionResult(int requestCode,
      String[] permissions, int[] grantResults) {
    if (requestCode != WITH_PERMISSIONS_REQUEST_CODE) return false;

    String[] waitingPermissions = this.waitingPermissions;
    Runnable codeWaitingForPermissions = this.codeWaitingForPermissions;

    this.waitingPermissions = null;
    this.codeWaitingForPermissions = null;

    if (waitingPermissions == null || codeWaitingForPermissions == null || !Arrays.equals(
        permissions, waitingPermissions)) {
      Log.e(TAG, "onRequestPermissionsResult: withPermissions() internal error.");
      Log.e(TAG, "waitingPermissions = " + Arrays.toString(waitingPermissions));
      Log.e(TAG, "codeWaitingForPermissions = " + codeWaitingForPermissions);
      Log.e(TAG, "waitingPermissions = " + Arrays.toString(waitingPermissions));
      Log.e(TAG, "resulting permissions = " + Arrays.toString(permissions));
      return true;
    }
    if (!ArrayUtils.allEqual(grantResults, PackageManager.PERMISSION_GRANTED)) {
      Log.e(TAG, "onRequestPermissionsResult: permissions rejected by user");
      return true;
    }

    codeWaitingForPermissions.run();

    return true;
  }

  protected Navigator createNavigator() {
    return null;
  }

  public Navigator getNavigator() {
    return navigator;
  }

  protected abstract @LayoutRes int getLayoutId();
}
