package ilya.myasoedov.revoluttest.ui.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.navigation.FragmentNavigationUtils;
import ilya.myasoedov.revoluttest.navigation.FragmentNavigatorImpl;
import ilya.myasoedov.revoluttest.navigation.Navigator;
import ilya.myasoedov.revoluttest.ui.base.fragment.OnBackPressedListener;
import java.util.List;

public abstract class BaseFragmentActivity extends BaseActivity{

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    getNavigator().setRoot(getRootFragment());
  }

  @Override protected int getLayoutId() {
    return R.layout.container_layout;
  }

  @Override protected Navigator createNavigator() {
    return new FragmentNavigatorImpl(this, getSupportFragmentManager());
  }

  @Override public void onBackPressed() {
    FragmentManager fragmentManager = getSupportFragmentManager();
    if (fragmentManager != null) {
      List<Fragment> fragments = fragmentManager.getFragments();
      if (fragments != null) {
        for (Fragment fragment : fragments) {
          if (fragment != null
              && fragment instanceof OnBackPressedListener
              && fragment.isVisible()) {
            ((OnBackPressedListener) fragment).onBackPressed();
          }
        }
      }
    }
  }

  @Override protected void onResume() {
    super.onResume();

    Fragment topFragment = getCurrentFragment();
    if (topFragment != null) {
      topFragment.onResume();
    }
  }

  protected Fragment getCurrentFragment() {
    return FragmentNavigationUtils.getTopFragment(getSupportFragmentManager());
  }

  protected abstract Fragment getRootFragment();
}
