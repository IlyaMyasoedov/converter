package ilya.myasoedov.revoluttest.ui.base.activity;

import butterknife.BindView;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.ui.view.ProgressView;

public abstract class ProgressFragmentActivity extends BaseFragmentActivity {
  @BindView(R.id.pvProgress) ProgressView pvProgress;

  public void setProgressState(boolean progress) {
    if (progress) {
      pvProgress.show();
    } else {
      pvProgress.hide();
    }
  }

  @Override protected int getLayoutId() {
    return R.layout.activity_progress_fragment;
  }
}
