package ilya.myasoedov.revoluttest.ui.base.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ilya.myasoedov.revoluttest.navigation.FragmentNavigatorImpl;
import ilya.myasoedov.revoluttest.navigation.Navigator;
import ilya.myasoedov.revoluttest.navigation.callback.NavigatorCallback;
import ilya.myasoedov.revoluttest.ui.base.activity.ProgressFragmentActivity;
import ilya.myasoedov.revoluttest.utils.KeyboardUtils;

public abstract class BaseFragment extends Fragment implements OnBackPressedListener,
    NavigatorCallback {
  private Unbinder unbinder;

  private Navigator navigator;

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(getLayoutId(), container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
    initializeInjector();
    navigator = createNavigator();
    if (getNavigator() != null) {
      getNavigator().setNavigatorCallback(this);
    }
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public void onDestroyView() {
    super.onDestroyView();

    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  @Override public void onBackPressed() {
    back();
  }

  public void hideKeyboard() {
    KeyboardUtils.hideKeyboard(getActivity());
  }

  /**
   * Show
   */
  public void setProgressState(boolean progress) {
    if (getActivity() instanceof ProgressFragmentActivity) {
      ((ProgressFragmentActivity) getActivity()).setProgressState(progress);
    }
  }

  public void setResult(int resultCode) {
    setResult(resultCode, null);
  }

  public void setResult(int resultCode, Intent data) {
    Navigator navigator = getNavigator();
    if (navigator != null) {
      navigator.setResult(getTargetRequestCode(), resultCode, data, this);
    }
  }

  public void setResult(int requestCode, int resultCode, Intent data){
    Navigator navigator = getNavigator();
    if (navigator != null) {
      navigator.setResult(requestCode, resultCode, data, this);
    }
  }

  /**
   * If you want to change the implementation, you can override this method and return your
   * implementation {@link Navigator}
   */
  protected Navigator createNavigator() {
    if (getParentFragment() != null && getParentFragment() instanceof BaseFragment) {
      return ((BaseFragment) getParentFragment()).getNavigator();
    }

    if (getActivity() instanceof ProgressFragmentActivity) {
      return ((ProgressFragmentActivity) getActivity()).getNavigator();
    }

    return null;
  }

  public Navigator getNavigator(){
    if (navigator == null) {
      return new FragmentNavigatorImpl(getActivity(), getChildFragmentManager());
    }

    return navigator;
  }

  public void back(){
    if (navigator != null){
      navigator.back();
    } else {
      getActivity().finish();
    }
  }

  @Override public void openCallback() {
    mandatoryActions();
  }

  @Override public void backCallback() {
    mandatoryActions();
  }

  private void mandatoryActions() {
    hideKeyboard();
    setProgressState(false);
  }

  protected abstract @LayoutRes int getLayoutId();

  /**
   * Init dagger component
   */
  protected abstract void initializeInjector();
}
