package ilya.myasoedov.revoluttest.ui.base.fragment;

public interface OnBackPressedListener {
  void onBackPressed();
}
