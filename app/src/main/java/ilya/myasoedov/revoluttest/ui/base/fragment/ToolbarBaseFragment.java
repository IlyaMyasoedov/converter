package ilya.myasoedov.revoluttest.ui.base.fragment;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import butterknife.BindView;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.utils.Fonts;
import ilya.myasoedov.revoluttest.utils.ToolbarUtils;

public abstract class ToolbarBaseFragment extends BaseFragment
    implements Toolbar.OnMenuItemClickListener{
  @Nullable @BindView(R.id.toolbar) protected Toolbar toolbar;
  @Nullable @BindView(R.id.vShadowToolbar) protected View vShadowToolbar;

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    initializeToolbar();
    setupFonts();
  }

  private void setupFonts(){
    if (toolbar != null) {
      ToolbarUtils.setTitleFont(Fonts.robotoRegular, toolbar);
      ToolbarUtils.setSubtitleFont(Fonts.robotoRegular, toolbar);
    }
  }

  private void initializeToolbar() {
    if (toolbar != null) {
      setTitle(getTitle());
    }
  }

  public String getTitleString(){
    if (toolbar == null || toolbar.getTitle() == null) {
      return null;
    }
    return toolbar.getTitle().toString();
  }

  protected void setTitle(@StringRes int titleRes) {
    if (toolbar != null && titleRes > 0) {
      toolbar.setTitle(titleRes);
    }
  }

  protected void setTitle(String title) {
    if (toolbar != null) {
      toolbar.setTitle(title);
    }
  }

  protected void setSubTitle(String subTitle) {
    if (toolbar != null) {
      toolbar.setSubtitle(subTitle);
    }
  }

  protected void setToolbarVisibility(boolean visible) {
    int visibility = visible ? View.VISIBLE : View.GONE;

    if (toolbar != null) {
      toolbar.setVisibility(visibility);
    }

    if (vShadowToolbar != null) {
      vShadowToolbar.setVisibility(visibility);
    }
  }

  protected void showToolbarBackButton() {
    if (toolbar != null) {
      toolbar.setNavigationIcon(
          ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_back_white));
      toolbar.setNavigationOnClickListener(v -> back());
    }
  }

  protected void setNavigationButton(@DrawableRes int res) {
    setNavigationButton(res, null);
  }

  protected void setNavigationButton(@DrawableRes int res, View.OnClickListener onClickListener) {
    if (toolbar != null) {
      toolbar.setNavigationIcon(ContextCompat.getDrawable(getActivity(), res));
      if (onClickListener != null) {
        toolbar.setNavigationOnClickListener(onClickListener);
      } else {
        toolbar.setNavigationOnClickListener(v -> back());
      }
    }
  }

  protected void inflateMenu(@MenuRes int resId) {
    if (toolbar != null && resId > 0) {
      toolbar.inflateMenu(resId);
      toolbar.setOnMenuItemClickListener(this);
    }
  }

  protected MenuItem findItem(int id) {
    if (toolbar != null) {
      return toolbar.getMenu().findItem(id);
    }

    return null;
  }

  protected void setToolbarOnClick(View.OnClickListener onClickListener) {
    if (toolbar != null) {
      toolbar.setOnClickListener(onClickListener);
    }
  }

  protected Menu getToolbarMenu() {
    if (toolbar != null) {
      return toolbar.getMenu();
    }

    return null;
  }

  protected String getSubTitle() {
    if (toolbar != null && !TextUtils.isEmpty(toolbar.getSubtitle())) {
      return toolbar.getSubtitle().toString();
    }

    return "";
  }

  protected void setMenuText(String text, int id) {
    MenuItem menu = findItem(id);
    if (menu != null) {
      menu.setTitle(text);
    }
  }

  protected void setVisibilityMenu(boolean visisble, int id) {
    MenuItem menu = findItem(id);
    if (menu != null) {
      menu.setVisible(visisble);
    }
  }

  protected void setMenuEnabled(boolean enabled, int id) {
    MenuItem menu = findItem(id);
    if (menu != null) {
      menu.setEnabled(enabled);
    }
  }

  @Override public boolean onMenuItemClick(MenuItem item) {
    return false;
  }

  protected abstract @StringRes int getTitle();
}
