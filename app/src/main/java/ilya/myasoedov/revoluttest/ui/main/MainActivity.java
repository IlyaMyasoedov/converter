package ilya.myasoedov.revoluttest.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import ilya.myasoedov.revoluttest.ui.base.activity.ProgressFragmentActivity;

public class MainActivity extends ProgressFragmentActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (savedInstanceState == null) {
      setupView();
    }
  }

  @Override protected Fragment getRootFragment() {
    return null;
  }

  private void setupView() {
    getNavigator().openMain();
  }
}
