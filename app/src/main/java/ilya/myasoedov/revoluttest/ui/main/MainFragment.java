package ilya.myasoedov.revoluttest.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import ilya.myasoedov.revoluttest.data.model.CurrencyData;
import ilya.myasoedov.revoluttest.ui.mvp.fragment.ToolbarBaseFragmentMvp;
import ilya.myasoedov.revoluttest.ui.view.currency.CurrencyView;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.CurrencyViewListener;
import ilya.myasoedov.revoluttest.utils.App;
import ilya.myasoedov.revoluttest.utils.CollectionUtils;
import ilya.myasoedov.revoluttest.utils.Currency;
import ilya.myasoedov.revoluttest.utils.KeyboardUtils;
import ilya.myasoedov.revoluttest.utils.PopupUtils;
import ilya.myasoedov.revoluttest.utils.ReconstructUtils;
import java.util.List;
import javax.inject.Inject;

public class MainFragment extends ToolbarBaseFragmentMvp<MainView, MainPresenter>
    implements MainView, CurrencyViewListener {

  @BindView(R.id.cvView) protected CurrencyView cvView;

  @BindView(R.id.tvRateOfExchange) TextView tvRateOfExchange;

  @Inject MainPresenter presenter;

  @OnClick(R.id.tvRateOfExchange) public void tvRateOfExchange() {

  }

  @Override public void currentRateString(String text) {
    tvRateOfExchange.setVisibility(View.VISIBLE);
    tvRateOfExchange.setText(text);
  }

  @Override public void hideRate(){
    tvRateOfExchange.setVisibility(View.GONE);
  }

  @Override public void latestLoaded(LatestLocal latestLocal, boolean refresh) {
    List<CurrencyData> items = ReconstructUtils.toCurrencyData(latestLocal);
    if (!CollectionUtils.isEmpty(items)) {
      if (refresh) {
        cvView.setupCurrencyData(items);
      } else {
        cvView.updateCurrencyData(items);
      }
    }
  }

  @Override public void setRefreshing(boolean refreshing) {
    setProgressState(refreshing);
  }

  @Override public void initializeInjector() {
    App.getInstance().getComponentManager().getMainComponent().inject(this);
  }

  @Override public void showError(String error) {
    PopupUtils.showMessage(getView(), error);
  }

  @Override public int getTitle() {
    return 0;
  }

  @Override public int getLayoutId() {
    return R.layout.fragment_main;
  }

  @NonNull @Override public MainPresenter createPresenter() {
    return presenter;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setupToolbar();
    setupView();

    presenter.getLatest(Currency.USD, true);
  }

  private void setupToolbar() {
    setNavigationButton(R.drawable.ic_arrow_back_white);
  }

  private void setupView() {
    cvView.bind(getChildFragmentManager());
    cvView.setCurrencyListener(this);
  }
}
