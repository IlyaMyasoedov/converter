package ilya.myasoedov.revoluttest.ui.main;

import android.content.Context;
import android.os.Handler;
import ilya.myasoedov.revoluttest.data.db.DatabaseUtils;
import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import ilya.myasoedov.revoluttest.ui.mvp.BasePresenter;
import ilya.myasoedov.revoluttest.ui.mvp.observers.ObserverWithErrorMessage;
import rx.Subscription;

import static ilya.myasoedov.revoluttest.utils.AppsUtils.POLLING_DELAY;

public class MainPresenter extends BasePresenter<MainView> {
  private MainRepository repository;
  private final Handler HANDLER;

  public MainPresenter(Context context, MainRepository repository) {
    super(context);
    this.repository = repository;
    this.HANDLER = new Handler();
  }

  /**
   * method to get the latest data
   *
   * @param base base currency
   * @param refresh true if there are no available data; otherwise false
   */
  public void getLatest(String base, boolean refresh) {
    MainView mainView = getView();
    if (mainView != null) {
      final LatestLocal cached = getFromDbIfExists();
      if (refresh && cached != null) {
        mainView.latestLoaded(cached, true);
      }
      HANDLER.postDelayed(() -> getLatest(base, false), POLLING_DELAY);
      Subscription subscription = repository.getLatest(base).doOnSubscribe(() -> {
        if (cached == null) mainView.setRefreshing(true);
      }).doOnTerminate(() -> {
        if (cached == null) mainView.setRefreshing(false);
      }).subscribe(new ObserverWithErrorMessage<LatestLocal>(mainView) {
        @Override public void onNext(LatestLocal latestLocal) {
          mainView.latestLoaded(latestLocal, needToRefresh(cached, refresh));
          DatabaseUtils.saveLatest(latestLocal, database);
        }
      });
      compositeSubscription.add(subscription);
    }
  }

  private boolean needToRefresh(LatestLocal cached, boolean refresh){
    boolean needToRefresh = refresh;
    if (cached != null && refresh) {
      needToRefresh = false;
    }
    return needToRefresh;
  }

  private LatestLocal getFromDbIfExists() {
    LatestLocal latestLocal = DatabaseUtils.getLatest(database);
    return latestLocal;
  }
}
