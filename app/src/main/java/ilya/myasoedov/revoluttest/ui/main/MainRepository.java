package ilya.myasoedov.revoluttest.ui.main;

import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import ilya.myasoedov.revoluttest.network.api.Api;
import rx.Observable;

public class MainRepository {
  private Api api;

  public MainRepository(Api api){
    this.api = api;
  }

  public Observable<LatestLocal> getLatest(String base){
    return api.getLatest(base);
  }
}
