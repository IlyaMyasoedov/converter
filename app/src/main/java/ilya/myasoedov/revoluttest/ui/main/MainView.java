package ilya.myasoedov.revoluttest.ui.main;

import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import ilya.myasoedov.revoluttest.ui.mvp.MvpViewWithProgress;

public interface MainView extends MvpViewWithProgress {
  void latestLoaded(LatestLocal latestLocal, boolean refresh);
}
