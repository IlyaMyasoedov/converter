package ilya.myasoedov.revoluttest.ui.mvp;

import android.content.Context;
import com.hannesdorfmann.mosby.mvp.MvpView;
import ilya.myasoedov.revoluttest.data.db.Database;
import rx.subscriptions.CompositeSubscription;

public class BasePresenter<V extends MvpView> extends MvpPresenterLifecycled<V> {
  protected Context context;
  protected CompositeSubscription compositeSubscription;
  protected Database<Long> database;

  public BasePresenter(Context context) {
    this.context = context;
    this.database = Database.INSTANCE;
    this.compositeSubscription = new CompositeSubscription();
  }

  @Override public void detachView(boolean retainInstance) {
    super.detachView(retainInstance);
    this.compositeSubscription.clear();
  }
}
