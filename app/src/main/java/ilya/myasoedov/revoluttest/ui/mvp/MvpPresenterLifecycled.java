package ilya.myasoedov.revoluttest.ui.mvp;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;

public class MvpPresenterLifecycled<V extends MvpView> extends MvpBasePresenter<V> {
  public void resumeView() {
  }

  public void pauseView() {
  }

  public void startView() {
  }

  public void stopView() {
  }

  public void destroyView() {
  }
}
