package ilya.myasoedov.revoluttest.ui.mvp;

import android.content.Context;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface MvpViewBase extends MvpView {
  void showError(String error);
  Context getContext();
}
