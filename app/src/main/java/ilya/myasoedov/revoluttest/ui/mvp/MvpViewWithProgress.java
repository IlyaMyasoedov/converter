package ilya.myasoedov.revoluttest.ui.mvp;


public interface MvpViewWithProgress extends MvpViewBase{
  void setRefreshing(boolean progress);
}
