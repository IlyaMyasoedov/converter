package ilya.myasoedov.revoluttest.ui.mvp.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;
import com.hannesdorfmann.mosby.mvp.delegate.BaseMvpDelegateCallback;
import com.hannesdorfmann.mosby.mvp.delegate.FragmentMvpDelegate;
import com.hannesdorfmann.mosby.mvp.delegate.FragmentMvpDelegateImpl;
import ilya.myasoedov.revoluttest.ui.base.fragment.ToolbarBaseFragment;
import ilya.myasoedov.revoluttest.ui.mvp.MvpPresenterLifecycled;

/**
 * If your fragment not use {@link Toolbar} then use {@link BaseFragmentMvp}
 * @param <V>
 * @param <P>
 */
public abstract class ToolbarBaseFragmentMvp<V extends MvpView, P extends MvpPresenter<V>>
    extends ToolbarBaseFragment implements BaseMvpDelegateCallback<V, P>, MvpView {

  protected FragmentMvpDelegate<V, P> mvpDelegate;
  protected P presenter;

  /**
   * Creates a new presenter instance, if needed. Will reuse the previous presenter instance if
   * {@link #setRetainInstance(boolean)} is set to true. This method will be called from
   * {@link #onViewCreated(View, Bundle)}
   */
  //    @NonNull
  //    public abstract P createPresenter();
  @NonNull protected FragmentMvpDelegate<V, P> getMvpDelegate() {
    if (mvpDelegate == null) mvpDelegate = new FragmentMvpDelegateImpl<>(this);
    return mvpDelegate;
  }

  @NonNull @Override public P getPresenter() {
    return presenter;
  }

  @Override public void setPresenter(@NonNull P presenter) {
    this.presenter = presenter;
  }

  @Override public boolean isRetainInstance() {
    return getRetainInstance();
  }

  @Override public boolean shouldInstanceBeRetained() {
    FragmentActivity activity = getActivity();
    boolean changingConfig = activity != null && activity.isChangingConfigurations();
    return getRetainInstance() && changingConfig;
  }

  @NonNull @Override public V getMvpView() {
    return (V) this;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getMvpDelegate().onViewCreated(view, savedInstanceState);
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    getMvpDelegate().onDestroyView();
    // added for additional interactions with presenter
    if (presenter instanceof MvpPresenterLifecycled) {
      ((MvpPresenterLifecycled) presenter).destroyView();
    }
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getMvpDelegate().onCreate(savedInstanceState);
  }

  @Override public void onDestroy() {
    super.onDestroy();
    getMvpDelegate().onDestroy();
  }

  @Override public void onPause() {
    super.onPause();
    getMvpDelegate().onPause();
    // added for additional interactions with presenter
    if (presenter instanceof MvpPresenterLifecycled) {
      ((MvpPresenterLifecycled) presenter).pauseView();
    }
  }

  @Override public void onResume() {
    super.onResume();
    getMvpDelegate().onResume();
    if (presenter instanceof MvpPresenterLifecycled) {
      ((MvpPresenterLifecycled) presenter).resumeView();
    }
  }

  @Override public void onStart() {
    super.onStart();
    getMvpDelegate().onStart();
    if (presenter instanceof MvpPresenterLifecycled) {
      ((MvpPresenterLifecycled) presenter).startView();
    }
  }

  @Override public void onStop() {
    super.onStop();
    getMvpDelegate().onStop();
    if (presenter instanceof MvpPresenterLifecycled) {
      ((MvpPresenterLifecycled) presenter).stopView();
    }
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    getMvpDelegate().onActivityCreated(savedInstanceState);
  }

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    getMvpDelegate().onAttach(activity);
  }

  @Override public void onDetach() {
    super.onDetach();
    getMvpDelegate().onDetach();
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    getMvpDelegate().onSaveInstanceState(outState);
  }
}
