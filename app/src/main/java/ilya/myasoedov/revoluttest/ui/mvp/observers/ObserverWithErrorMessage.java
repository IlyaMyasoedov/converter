package ilya.myasoedov.revoluttest.ui.mvp.observers;

import android.support.annotation.StringRes;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.ui.mvp.MvpViewWithProgress;
import java.lang.ref.WeakReference;

public class ObserverWithErrorMessage<T> extends SimpleObserver<T> {
  protected final WeakReference<MvpViewWithProgress> view;
  @StringRes private int errorMessage;

  public ObserverWithErrorMessage(MvpViewWithProgress view) {
    this.view = new WeakReference<>(view);
    this.errorMessage = R.string.error_occurred;
  }

  public ObserverWithErrorMessage(MvpViewWithProgress view, @StringRes int errorMessage) {
    this.view = new WeakReference<>(view);
    this.errorMessage = errorMessage;
  }

  @Override public void onError(Throwable e) {
    MvpViewWithProgress v = view.get();
    if (v != null) {
      String msg = v.getContext().getString(errorMessage);
      v.setRefreshing(false);
      v.showError(msg);
    }
  }
}
