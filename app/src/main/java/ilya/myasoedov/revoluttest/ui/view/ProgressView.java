package ilya.myasoedov.revoluttest.ui.view;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class ProgressView extends LinearLayout {
  private final static int BACKGROUND_COLOR = 0xD9FFFFFF;

  public ProgressView(Context context) {
    this(context, null);
  }

  public ProgressView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    setLayoutParams(
        new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    setBackgroundColor(BACKGROUND_COLOR);
    setGravity(Gravity.CENTER);
    setOrientation(VERTICAL);
    setVisibility(GONE);

    ProgressBar progressBar = new ProgressBar(getContext());
    progressBar.setLayoutParams(
        new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    addView(progressBar);
  }

  public void show() {
    setBackgroundColor(BACKGROUND_COLOR);
    setVisibility(VISIBLE);
  }

  public void hide() {
    setVisibility(GONE);
  }

  public void show(@ColorInt int color) {
    setBackgroundColor(color);
    setVisibility(VISIBLE);
  }
}
