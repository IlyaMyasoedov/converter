package ilya.myasoedov.revoluttest.ui.view.currency;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CurrencyAdapter extends FragmentPagerAdapter {
  private final List<Fragment> fragmentList = new ArrayList<>();
  private final Map<Integer, String> fragmentTags = new HashMap<>();
  private FragmentManager fragmentManager;

  public CurrencyAdapter(FragmentManager manager) {
    super(manager);
    this.fragmentManager = manager;
  }

  @Override public Fragment getItem(int position) {
    return fragmentList.get(position);
  }

  @Override public int getCount() {
    return fragmentList.size();
  }

  @Override public Object instantiateItem(ViewGroup container, int position) {
    Object object = super.instantiateItem(container, position);
    if (object instanceof Fragment) {
      Fragment fragment = (Fragment) object;
      String tag = fragment.getTag();
      fragmentTags.put(position, tag);
    }
    return object;
  }

  public void addFragment(Fragment fragment) {
    fragmentList.add(fragment);
  }

  @Nullable public Fragment getFragment(int position) {
    Fragment fragment = null;
    String tag = fragmentTags.get(position);
    if (tag != null && fragmentManager != null) {
      fragment = fragmentManager.findFragmentByTag(tag);
    }
    return fragment;
  }
}
