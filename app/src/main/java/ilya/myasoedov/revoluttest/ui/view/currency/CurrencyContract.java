package ilya.myasoedov.revoluttest.ui.view.currency;

import android.support.v4.app.FragmentManager;
import ilya.myasoedov.revoluttest.data.model.CurrencyData;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.CurrencyViewListener;
import java.util.List;

public interface CurrencyContract {
  void bind(FragmentManager manager);

  void setupCurrencyData(List<CurrencyData> datas);

  void updateCurrencyData(List<CurrencyData> datas);

  void setCurrencyListener(CurrencyViewListener listener);
}
