package ilya.myasoedov.revoluttest.ui.view.currency;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.ui.view.currency.filter.CurrencyFilter;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.WatcherCallback;
import ilya.myasoedov.revoluttest.utils.KeyboardUtils;

public class CurrencyEditView extends RelativeLayout implements WatcherCallback {
  @BindView(R.id.tvCurrentCurrency) protected TextView tvCurrentCurrency;
  @BindView(R.id.tvSign1) protected TextView tvSign1;
  @BindView(R.id.tvSign2) protected TextView tvSign2;

  @BindView(R.id.etQuantity1) protected EditText etQuantity1;
  @BindView(R.id.etQuantity2) protected EditText etQuantity2;

  private CurrencyTextWatcher textWatcher = null;
  private WatcherCallback callback;

  public CurrencyEditView(Context context) {
    this(context, null);
  }

  public CurrencyEditView(Context context, AttributeSet attr) {
    this(context, attr, 0);
  }

  public CurrencyEditView(Context context, AttributeSet attr, int defStyleAttr) {
    super(context, attr, defStyleAttr);
    setupView();
  }

  @Override public void afterCurrencyChanged(double value) {
    if (callback != null) {
      callback.afterCurrencyChanged(value);
    }
  }

  @Override public void newFocus(){
    if (callback != null) {
      callback.newFocus();
    }
  }

  private void setupView() {
    final View view =
        LayoutInflater.from(getContext()).inflate(R.layout.view_currency_edit, this, true);
    ButterKnife.bind(this, view);

    setupViews();
    setupCallbacks();
  }

  private void setupViews() {
    textWatcher =
        new CurrencyTextWatcher(etQuantity1, etQuantity2, tvCurrentCurrency, tvSign1, tvSign2);
    etQuantity1.addTextChangedListener(textWatcher);
    etQuantity1.setFilters(new InputFilter[] { new CurrencyFilter() });
    etQuantity2.setFilters(new InputFilter[] { new CurrencyFilter() });
  }

  private void setupCallbacks() {
    textWatcher.setCallback(this);
  }

  public void setCallback(WatcherCallback callback) {
    this.callback = callback;
  }

  public void setCurrency(String currency) {
    tvCurrentCurrency.setText(currency);
  }

  public void setCurrencyValue(double value) {
    textWatcher.setCurrencyValue(value);
  }

  public double getCurrentValue(){
    return textWatcher.getCurrentValue();
  }

  /**
   * set the type of currency fragment; there are two types - top and bottom
   */
  public void setIsBottom(boolean isBottom) {
    if (textWatcher != null) {
      textWatcher.plus(isBottom);
    }
  }
}
