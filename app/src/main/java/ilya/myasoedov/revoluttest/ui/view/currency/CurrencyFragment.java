package ilya.myasoedov.revoluttest.ui.view.currency;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.data.model.CurrencyData;
import ilya.myasoedov.revoluttest.ui.base.fragment.BaseFragment;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.CurrencyFragmentListener;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.WatcherCallback;
import ilya.myasoedov.revoluttest.utils.ConverterUtils;
import ilya.myasoedov.revoluttest.utils.CurrencyUtils;

import static ilya.myasoedov.revoluttest.navigation.RequestKey.BASE_CURRENCY;
import static ilya.myasoedov.revoluttest.navigation.RequestKey.CURRENCY_DATA;
import static ilya.myasoedov.revoluttest.navigation.RequestKey.IS_BOTTOM;
import static ilya.myasoedov.revoluttest.utils.ConverterUtils.formRateOfExchangeString;
import static ilya.myasoedov.revoluttest.utils.ConverterUtils.getRateOfExchange;

public class CurrencyFragment extends Fragment implements WatcherCallback {

  @BindView(R.id.cevView) protected CurrencyEditView cevView;

  @BindView(R.id.tvYouHave) protected TextView tvYouHave;
  @BindView(R.id.tvRateOfExchange) protected TextView tvRateOfExchange;

  private Unbinder unbinder;

  private CurrencyFragmentListener callback;

  private CurrencyData data;
  private boolean isBottom = false;
  private boolean focusInBottom = false;
  private String baseCurrency;

  public static CurrencyFragment newInstance(CurrencyData data, String baseCurrency,
      boolean isBottom) {
    CurrencyFragment fragment = new CurrencyFragment();
    CurrencyUtils.addToBundle(fragment, data, baseCurrency, isBottom);
    return fragment;
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_currency, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    Bundle bundle = getArguments();
    if (bundle != null) {
      data = bundle.getParcelable(CURRENCY_DATA);
      isBottom = bundle.getBoolean(IS_BOTTOM);
      baseCurrency = bundle.getString(BASE_CURRENCY);
    }

    updateData();
    setupCallbacks();
  }

  @Override public void afterCurrencyChanged(double value) {
    if (callback != null && data != null) {
      callback.afterCurrencyChanged(data.getCurrency(), value);
    }
    updateYouHaveColor();
  }

  @Override public void newFocus() {
    if (callback != null) {
      callback.newFocus(isBottom);
    }
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  public void setData(CurrencyData data, String baseCurrency, boolean isBottom) {
    CurrencyUtils.addToBundle(this, data, baseCurrency, isBottom);
    this.baseCurrency = baseCurrency;
    this.data = data;
    this.isBottom = isBottom;
    updateData();
  }

  public void updateCurrency(String baseCurrency) {
    CurrencyUtils.addToBundle(this, data, baseCurrency, isBottom);
    this.baseCurrency = baseCurrency;
    updateData();
  }

  public void updateFocusInBottom(boolean focusInBottom){
    this.focusInBottom = focusInBottom;
    updateData();
  }

  public void setCurrencyValue(String currency, double value) {
    if (data != null) {
      cevView.setCurrencyValue(ConverterUtils.value(data, currency, value));
    }
  }

  public CurrencyData getData() {
    return data;
  }

  public void setCallback(CurrencyFragmentListener callback) {
    this.callback = callback;
  }

  private void updateData() {
    if (data != null) {
      cevView.setCurrency(data.getCurrency());
      cevView.setIsBottom(isBottom);
      CurrencyUtils.setupYouHave(tvYouHave, ConverterUtils.youHave(data), data.getCurrency());
      updateYouHaveColor();
      if (canShowRate() && checkCurrency()) {
        tvRateOfExchange.setVisibility(View.VISIBLE);
        tvRateOfExchange.setText(formRateOfExchangeString(getContext(), data, baseCurrency));
      } else {
        tvRateOfExchange.setVisibility(View.GONE);
      }
    }
  }

  private void updateYouHaveColor() {
    if (data != null) {
      double currentValue = cevView.getCurrentValue();
      double youHave = ConverterUtils.youHave(data);
      tvYouHave.setTextColor(ContextCompat.getColor(getContext(),
          currentValue > youHave ? R.color.red : R.color.white));
    }
  }

  private boolean canShowRate(){
    return (isBottom && !focusInBottom || !isBottom && focusInBottom);
  }

  private boolean checkCurrency() {
    return !TextUtils.isEmpty(baseCurrency) && !baseCurrency.equals(data.getCurrency());
  }

  private void setupCallbacks() {
    cevView.setCallback(this);
  }
}
