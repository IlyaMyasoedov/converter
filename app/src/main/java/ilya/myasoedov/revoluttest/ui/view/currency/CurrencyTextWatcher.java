package ilya.myasoedov.revoluttest.ui.view.currency;

import android.renderscript.Double2;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.WatcherCallback;
import ilya.myasoedov.revoluttest.ui.view.currency.state.State;
import ilya.myasoedov.revoluttest.utils.KeyboardUtils;
import ilya.myasoedov.revoluttest.utils.UiUtils;

import static ilya.myasoedov.revoluttest.utils.CurrencyUtils.defaultFormat;
import static ilya.myasoedov.revoluttest.utils.StringUtils.removeTrailingZeros;

class CurrencyTextWatcher implements TextWatcher, View.OnTouchListener, View.OnFocusChangeListener {
  private final static String ZERO = "0";
  private final static String PLUS = "+";
  private final static String MINUS = "-";
  private final static String EMPTY = "";

  private State state = State.BIG;

  /**
   * big edittext
   */
  private EditText target1;
  /**
   * small edittext
   */
  private EditText target2;

  /**
   * sign of target1
   */
  private TextView tvSign1;

  /**
   * sign of target2
   */
  private TextView tvSign2;

  /**
   * view that observes if editexts are overlapping this view or not
   */
  private View observer;

  /**
   * length of small edittext when it changes its visibility
   */
  private int pivotLength;

  /**
   * string length before changes
   */
  private int startLength = 0;

  /**
   * prefix value; true - "+"; false - "-"
   */
  private boolean plus = false;

  private WatcherCallback callback;

  /**
   * @param target1 big edittext
   * @param target2 small edittext
   * @param observer view right from edittext
   */
  public CurrencyTextWatcher(EditText target1, EditText target2, View observer, TextView tvSign1,
      TextView tvSign2) {
    this.target1 = target1;
    this.target2 = target2;
    this.observer = observer;
    this.tvSign1 = tvSign1;
    this.tvSign2 = tvSign2;
    setupListeners();
  }

  @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    startLength = s.length();
  }

  @Override public void onTextChanged(CharSequence s, int start, int before, int count) {

  }

  @Override public void afterTextChanged(Editable s) {
    if (state == State.BIG) {
      processBigState(s);
    } else if (state == State.SMALL) {
      processSmallState(s);
    }
    callback(s);
  }

  @Override public void onFocusChange(View view, boolean hasFocus){
    if (hasFocus && callback != null) {
      callback.newFocus();
    }
  }

  private void switchState(EditText from, EditText to, View signFrom, View signTo, Editable s) {
    to.setVisibility(View.VISIBLE);
    from.setVisibility(View.GONE);

    //to.requestFocus();
    to.setText(s);
    to.setSelection(s.length());

    from.removeTextChangedListener(this);
    to.addTextChangedListener(this);

    if (s.length() > 0) {
      signFrom.setVisibility(View.GONE);
      signTo.setVisibility(View.VISIBLE);
    }
  }

  private void callback(Editable s){
    if (callback != null) {
      String text = s.toString();
      try {
        double value = TextUtils.isEmpty(text) ? 0.0 : Double.parseDouble(text);
        callback.afterCurrencyChanged(value);
      } catch (Exception ex) {
      }
    }
  }

  private void processBigState(Editable s) {
    boolean symbolAdded = isSymbolAdded(s.toString());

    if (UiUtils.isViewOverlapping(target1, observer) && symbolAdded) {
      state = State.SMALL;
      pivotLength = s.length();
      switchState(target1, target2, tvSign1, tvSign2, s);
    } else if (s.length() == 0) {
      tvSign1.setVisibility(View.VISIBLE);
      tvSign1.setText(ZERO);
    } else if (s.length() >= 1) {
      String text = s.toString();
      if (s.length() == 1 && text.equals(ZERO)) {
        target1.setText(EMPTY);
        tvSign1.setText(ZERO);
      } else {
        tvSign1.setText(plus ? PLUS : MINUS);
      }
      tvSign1.setVisibility(View.VISIBLE);
    }
  }

  private void processSmallState(Editable s) {
    boolean symbolAdded = isSymbolAdded(s.toString());

    if (UiUtils.isViewOverlapping(target2, observer) && symbolAdded) {
      UiUtils.setMaxLength(target2, s.length());
    } else if (s.length() < pivotLength && !symbolAdded) {
      state = State.BIG;
      switchState(target2, target1, tvSign2, tvSign1, s);
    }
  }

  private boolean isSymbolAdded(String s) {
    if (TextUtils.isEmpty(s) || s.equals(ZERO)) {
      return false;
    }
    return s.length() - startLength > 0;
  }

  private void requestFocusAndShowKeyboard(View view) {
    view.requestFocus();
    KeyboardUtils.showKeyboard(view.getContext());
  }

  /**
   * method for prefix value;
   *
   * @param plus plus = true means prefix is "+"; otherwise prefix is "-"
   */
  public void plus(boolean plus) {
    this.plus = plus;
    this.tvSign2.setText(plus ? PLUS : MINUS);
  }

  public void setCallback(WatcherCallback callback) {
    this.callback = callback;
  }

  public void setCurrencyValue(double value) {
    String stringValue = removeTrailingZeros(String.valueOf(defaultFormat(value)));
    if (state == State.BIG) {
      target1.setText(stringValue);
    } else {
      target2.setText(stringValue);
    }
  }

  public double getCurrentValue() {
    String text;
    if (state == State.BIG) {
      text = target1.getText().toString();
    } else {
      text = target2.getText().toString();
    }
    try{
      return Double.parseDouble(text);
    }catch (Exception ex){
      return 0.0d;
    }
  }

  @Override public boolean onTouch(View view, MotionEvent motionEvent) {
    int action = motionEvent.getAction();
    if (action == MotionEvent.ACTION_DOWN) {
      int id = view.getId();
      if (id == R.id.tvSign1) {
        requestFocusAndShowKeyboard(target1);
        return true;
      } else if (id == R.id.tvSign2) {
        requestFocusAndShowKeyboard(target2);
        return true;
      }
    }
    return false;
  }

  private void setupListeners() {
    tvSign1.setOnTouchListener(this);
    tvSign2.setOnTouchListener(this);
    target1.setOnFocusChangeListener(this);
    target2.setOnFocusChangeListener(this);
  }
}
