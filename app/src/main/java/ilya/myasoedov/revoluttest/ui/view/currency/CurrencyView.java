package ilya.myasoedov.revoluttest.ui.view.currency;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.viewpagerindicator.CirclePageIndicator;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.data.model.CurrencyData;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.CurrencyFragmentListener;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.CurrencyViewListener;
import ilya.myasoedov.revoluttest.ui.view.currency.listeners.WatcherCallback;
import ilya.myasoedov.revoluttest.ui.view.currency.state.CurrencyViewState;
import ilya.myasoedov.revoluttest.utils.CollectionUtils;
import java.util.List;

import static ilya.myasoedov.revoluttest.utils.ConverterUtils.formRateOfExchangeString;
import static ilya.myasoedov.revoluttest.utils.ConverterUtils.invertOfExchangeString;

public class CurrencyView extends LinearLayout implements CurrencyContract {
  @BindView(R.id.vpPagerUp) protected ViewPager vpPagerUp;
  @BindView(R.id.vpPagerDown) protected ViewPager vpPagerDown;

  @BindView(R.id.cpiPagerUp) protected CirclePageIndicator cpiPagerUp;
  @BindView(R.id.cpiPagerDown) protected CirclePageIndicator cpiPagerDown;

  private CurrencyViewState state = CurrencyViewState.TOP_IN_FOCUS;

  private FragmentManager fragmentManager;
  private CurrencyAdapter adapter1 = null;
  private CurrencyAdapter adapter2 = null;
  private final Handler HANDLER = new Handler();

  private CurrencyViewListener listener;

  public CurrencyView(Context context) {
    this(context, null);
  }

  public CurrencyView(Context context, AttributeSet attr) {
    this(context, attr, 0);
  }

  public CurrencyView(Context context, AttributeSet attr, int defStyleAttr) {
    super(context, attr, defStyleAttr);
    setupView();
  }

  @Override public void bind(FragmentManager manager) {
    if (manager == null) {
      throw new RuntimeException("FragmentManager should not be null!");
    }
    this.fragmentManager = manager;
  }

  @Override public void setupCurrencyData(List<CurrencyData> items) {
    adapter1 = adapter(items, fragmentManager, false, "");
    adapter2 = adapter(items, fragmentManager, true,
        !CollectionUtils.isEmpty(items) ? items.get(0).getCurrency() : "");

    setupPager(adapter1, vpPagerUp, cpiPagerUp);
    setupPager(adapter2, vpPagerDown, cpiPagerDown);
  }

  @Override public void updateCurrencyData(List<CurrencyData> items) {
    updateCurrencyData(items, adapter1, false, currency());
    updateCurrencyData(items, adapter2, true, currency());
  }

  @Override public void setCurrencyListener(CurrencyViewListener listener) {
    this.listener = listener;
  }

  private String currency() {
    if (state == CurrencyViewState.TOP_IN_FOCUS) {
      CurrencyData data = data(adapter1, vpPagerUp.getCurrentItem());
      return data != null ? data.getCurrency() : "";
    } else {
      CurrencyData data = data(adapter2, vpPagerDown.getCurrentItem());
      return data != null ? data.getCurrency() : "";
    }
  }

  @Nullable private CurrencyData data(CurrencyAdapter adapter, int position) {
    CurrencyFragment fragment = ((CurrencyFragment) adapter.getFragment(position));
    if (fragment != null) {
      return fragment.getData();
    }

    return null;
  }

  private void resetItemInAdapter(int pos) {
    CurrencyAdapter adapter = state == CurrencyViewState.TOP_IN_FOCUS ? adapter1 : adapter2;
    HANDLER.postDelayed(() -> {
      CurrencyFragment fragment = ((CurrencyFragment) adapter.getFragment(pos));
      if (fragment != null) {
        fragment.setCurrencyValue("", 0);
      }
    }, 500);
  }

  private void processPager1(int previousPos, int position, CurrencyAdapter adapter1,
      CurrencyAdapter adapter2, ViewPager pager2) {
    if (adapter2 != null && adapter1 != null) {
      CurrencyFragment fragment1 = ((CurrencyFragment) adapter1.getFragment(position));
      CurrencyFragment fragment2 = ((CurrencyFragment) adapter2.getFragment(pager2.getCurrentItem()));
      CurrencyData data1 = fragment1 != null ? fragment1.getData() : null;
      CurrencyData data2 = fragment2 != null ? fragment2.getData() : null;
      if (data1 != null && data2 != null) {
        String currency1 = data1.getCurrency();
        String currency2 = data2.getCurrency();
        //reset previous value in adapter
        resetItemInAdapter(previousPos);

        updateCurrencyInPager(currency1);
        if (!TextUtils.isEmpty(currency1) && currency1.equals(currency2)) {
          listener.hideRate();
        } else {
          listener.currentRateString(invertOfExchangeString(getContext(), data2, currency1));
        }
      }
    }
  }

  private void processPager2(int position, CurrencyAdapter adapter1, ViewPager pager1,
      CurrencyAdapter adapter2) {
    if (adapter1 != null && adapter2 != null) {
      CurrencyFragment fragment1 = ((CurrencyFragment) adapter1.getFragment(pager1.getCurrentItem()));
      CurrencyFragment fragment2 = ((CurrencyFragment) adapter2.getFragment(position));
      CurrencyData data1 = fragment1 != null ? fragment1.getData() : null;
      CurrencyData data2 = fragment2 != null ? fragment2.getData() : null;
      if (data1 != null && data2 != null && listener != null) {
        String currency1 = data1.getCurrency();
        String currency2 = data2.getCurrency();
        if (!TextUtils.isEmpty(currency1) && currency1.equals(currency2)) {
          listener.hideRate();
        } else {
          listener.currentRateString(invertOfExchangeString(getContext(), data2, currency1));
        }
      }
    }
  }

  private void setupView() {
    final View view = LayoutInflater.from(getContext()).inflate(R.layout.view_currency, this, true);
    ButterKnife.bind(this, view);

    setupPagerListeners();
  }

  private void setupPager(CurrencyAdapter adapter, ViewPager pager, CirclePageIndicator indicator) {
    pager.setAdapter(adapter);
    indicator.setViewPager(pager);
    pager.setOffscreenPageLimit(3);
  }

  private void setupPagerListeners() {
    vpPagerUp.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
      private int prevPos = 0;

      @Override public void onPageSelected(int position) {
        processUpPager(prevPos, position);
        prevPos = position;
      }
    });
    vpPagerDown.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
      private int prevPos = 0;

      @Override public void onPageSelected(int position) {
        processDownPager(prevPos, position);
        prevPos = position;
      }
    });
  }

  private void processUpPager(int prevPosition, int position) {
    if (state == CurrencyViewState.TOP_IN_FOCUS) {
      processPager1(prevPosition, position, adapter1, adapter2, vpPagerDown);
    } else {
      processPager2(position, adapter2, vpPagerDown, adapter1);
    }
  }

  private void processDownPager(int prevPosition, int position) {
    if (state == CurrencyViewState.TOP_IN_FOCUS) {
      processPager2(position, adapter1, vpPagerUp, adapter2);
    } else {
      processPager1(prevPosition, position, adapter2, adapter1, vpPagerUp);
    }
  }

  private CurrencyAdapter adapter(List<CurrencyData> datas, FragmentManager fragmentManager,
      boolean isBottom, String currency) {
    CurrencyAdapter adapter = new CurrencyAdapter(fragmentManager);
    int size = datas.size();
    for (int position = 0; position < size; position++) {
      CurrencyFragment fragment =
          CurrencyFragment.newInstance(datas.get(position), currency, isBottom);
      if (isBottom) {
        fragment.setCallback(downCallback);
      } else {
        fragment.setCallback(upCallback);
      }
      adapter.addFragment(fragment);
    }
    return adapter;
  }

  private void updateCurrencyData(List<CurrencyData> items, CurrencyAdapter adapter,
      boolean isBottom, String currency) {
    if (!CollectionUtils.isEmpty(items) && adapter != null) {
      for (int position = 0; position < items.size(); position++) {
        CurrencyData data = items.get(position);
        CurrencyFragment fragment = (CurrencyFragment) adapter.getFragment(position);
        if (fragment != null) {
          fragment.setData(CurrencyData.copy(data), currency, isBottom);
        }
      }
    }
  }

  private void updateCurrencyInPager(String newCurrency) {
    CurrencyAdapter adapter = state == CurrencyViewState.TOP_IN_FOCUS ? adapter2 : adapter1;
    for (int position = 0; position < adapter.getCount(); position++) {
      CurrencyFragment fragment = (CurrencyFragment) adapter.getFragment(position);
      if (fragment != null) {
        fragment.updateCurrency(newCurrency);
      }
    }
  }

  private void updateFocus() {
    for (int position = 0; position < adapter1.getCount(); position++) {
      CurrencyFragment fragment1 = (CurrencyFragment) adapter1.getFragment(position);
      CurrencyFragment fragment2 = (CurrencyFragment) adapter2.getFragment(position);
      if (fragment1 != null) {
        fragment1.updateFocusInBottom(state == CurrencyViewState.BOTTOM_IN_FOCUS);
      }
      if (fragment2 != null) {
        fragment2.updateFocusInBottom(state == CurrencyViewState.BOTTOM_IN_FOCUS);
      }
    }
  }

  private void updateCurrencyValueInPager(String currency, double value) {
    CurrencyAdapter adapter = state == CurrencyViewState.TOP_IN_FOCUS ? adapter2 : adapter1;
    for (int position = 0; position < adapter.getCount(); position++) {
      CurrencyFragment fragment = (CurrencyFragment) adapter.getFragment(position);
      if (fragment != null) {
        fragment.setCurrencyValue(currency, value);
      }
    }
    updateFocus();
  }

  private final CurrencyFragmentListener upCallback = new CurrencyFragmentListener() {
    @Override public void afterCurrencyChanged(String currency, double value) {
      if (state == CurrencyViewState.TOP_IN_FOCUS) {
        updateCurrencyValueInPager(currency, value);
      }
    }

    @Override public void newFocus(boolean isBottom) {
      state = isBottom ? CurrencyViewState.BOTTOM_IN_FOCUS : CurrencyViewState.TOP_IN_FOCUS;
    }
  };

  private final CurrencyFragmentListener downCallback = new CurrencyFragmentListener() {
    @Override public void afterCurrencyChanged(String currency, double value) {
      if (state == CurrencyViewState.BOTTOM_IN_FOCUS) {
        updateCurrencyValueInPager(currency, value);
      }
    }

    @Override public void newFocus(boolean isBottom) {
      state = isBottom ? CurrencyViewState.BOTTOM_IN_FOCUS : CurrencyViewState.TOP_IN_FOCUS;
    }
  };
}
