package ilya.myasoedov.revoluttest.ui.view.currency.filter;

import android.text.InputFilter;
import android.text.Spanned;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CurrencyFilter implements InputFilter {
  private Pattern pattern;

  public CurrencyFilter() {
    pattern = Pattern.compile("^[0-9]+([.][0-9]{0,2})?$");
  }

  @Override
  public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart,
      int dend) {

    Matcher matcher = pattern.matcher(dest.toString() + source.toString());
    if (!matcher.matches()) return "";
    return null;
  }
}
