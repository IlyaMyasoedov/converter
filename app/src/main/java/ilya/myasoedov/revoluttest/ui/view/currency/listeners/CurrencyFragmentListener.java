package ilya.myasoedov.revoluttest.ui.view.currency.listeners;

public interface CurrencyFragmentListener {
  void afterCurrencyChanged(String currency, double value);
  void newFocus(boolean isBottom);
}
