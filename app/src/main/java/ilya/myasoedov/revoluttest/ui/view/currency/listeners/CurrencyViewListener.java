package ilya.myasoedov.revoluttest.ui.view.currency.listeners;


public interface CurrencyViewListener {
  void currentRateString(String rateOfExchange);
  void hideRate();
}
