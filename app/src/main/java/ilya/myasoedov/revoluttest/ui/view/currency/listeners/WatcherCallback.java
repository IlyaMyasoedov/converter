package ilya.myasoedov.revoluttest.ui.view.currency.listeners;


public interface WatcherCallback {
  void afterCurrencyChanged(double value);
  void newFocus();
}
