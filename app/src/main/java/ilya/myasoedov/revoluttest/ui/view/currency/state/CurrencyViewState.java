package ilya.myasoedov.revoluttest.ui.view.currency.state;

public enum CurrencyViewState {
  TOP_IN_FOCUS, BOTTOM_IN_FOCUS;
}
