package ilya.myasoedov.revoluttest.utils;

import android.app.Application;
import ilya.myasoedov.revoluttest.data.db.DatabaseHelperFactory;
import ilya.myasoedov.revoluttest.di.ComponentManager;
import ilya.myasoedov.revoluttest.di.components.AppComponent;
import ilya.myasoedov.revoluttest.di.components.DaggerAppComponent;
import ilya.myasoedov.revoluttest.di.modules.AppModule;

public class App extends Application{
  private static App app;
  private ComponentManager componentManager;

  @Override public void onCreate(){
    super.onCreate();
    DatabaseHelperFactory.setHelper(getApplicationContext());
    Fonts.loadAllFonts(getApplicationContext());
    buildAppComponent();

    app = this;
  }

  public static App getInstance() {
    return app;
  }

  private void buildAppComponent(){
    AppComponent appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    componentManager = new ComponentManager(appComponent);
  }

  public ComponentManager getComponentManager() {
    return componentManager;
  }
}
