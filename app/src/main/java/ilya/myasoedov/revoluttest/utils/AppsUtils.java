package ilya.myasoedov.revoluttest.utils;

import android.os.Build;

public class AppsUtils {

  private AppsUtils() {
    throw new AssertionError();
  }

  public static final boolean IS_LOLLIPOP = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
  public static final boolean IS_MARSHMALLOW = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
  public static final boolean IS_NOUGAT = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;

  public static final int POLLING_DELAY = 30 * 1000; //in mills

  public static final double USER_HAVE_IN_USD = 77.01; //we can't know this; that's why we are hardcoding

}
