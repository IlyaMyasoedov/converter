package ilya.myasoedov.revoluttest.utils;

public class ArrayUtils {
  public static boolean isEmpty(Object[] objects) {
    return objects == null || objects.length == 0;
  }

  public static boolean allEqual(int[] array, int value) {
    for (int a : array) {
      if (a != value) {
        return false;
      }
    }
    return true;
  }
}
