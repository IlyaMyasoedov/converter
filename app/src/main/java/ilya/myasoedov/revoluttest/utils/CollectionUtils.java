package ilya.myasoedov.revoluttest.utils;

import android.support.v4.util.ArrayMap;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class CollectionUtils {

  private CollectionUtils(){
    throw new AssertionError();
  }

  public static boolean isEmpty(Collection col) {
    return col == null || col.size() == 0;
  }

  public static <K, V> Map<K, V> mapOf(K key, V value) {
    return Collections.singletonMap(key, value);
  }

  public static <K, V> Map<K, V> mapOf(K k0, V v0, K k1, V v1) {
    ArrayMap<K, V> map = new ArrayMap<>(2);
    map.put(k0, v0);
    map.put(k1, v1);
    return map;
  }

  public static <K, V> Map<K, V> mapOf(K k0, V v0, K k1, V v1, K k2, V v2) {
    ArrayMap<K, V> map = new ArrayMap<>(3);
    map.put(k0, v0);
    map.put(k1, v1);
    map.put(k2, v2);
    return map;
  }

  public static <K, V> Map<K, V> mapOf(K k0, V v0, K k1, V v1, K k2, V v2, K k3, V v3) {
    ArrayMap<K, V> map = new ArrayMap<>(4);
    map.put(k0, v0);
    map.put(k1, v1);
    map.put(k2, v2);
    map.put(k3, v3);
    return map;
  }

  public static <K, V> Map<K, V> mapOf(K k0, V v0, K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
    ArrayMap<K, V> map = new ArrayMap<>(5);
    map.put(k0, v0);
    map.put(k1, v1);
    map.put(k2, v2);
    map.put(k3, v3);
    map.put(k4, v4);
    return map;
  }
}
