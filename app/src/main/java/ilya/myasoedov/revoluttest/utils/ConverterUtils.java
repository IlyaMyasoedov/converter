package ilya.myasoedov.revoluttest.utils;

import android.content.Context;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.data.model.CurrencyData;

import static ilya.myasoedov.revoluttest.utils.AppsUtils.USER_HAVE_IN_USD;
import static ilya.myasoedov.revoluttest.utils.CurrencyUtils.defaultFormat;
import static ilya.myasoedov.revoluttest.utils.CurrencyUtils.fourDigitsFormat;

public class ConverterUtils {

  private ConverterUtils() {
    throw new AssertionError();
  }

  /**
   * usd - base unit for computations
   */
  public static double youHave(CurrencyData data) {
    String base = data.getBase();
    String currentCurrency = data.getCurrency();
    try {
      if (Currency.EUR.equals(base)) {
        if (Currency.USD.equals(currentCurrency)) {
          return USER_HAVE_IN_USD / data.getUsd();
        } else if (Currency.GBP.equals(currentCurrency)) {
          return USER_HAVE_IN_USD * data.getGbp() / data.getUsd();
        } else if (Currency.EUR.equals(currentCurrency)) {
          return USER_HAVE_IN_USD * data.getEur() / data.getUsd();
        }
      } else if (Currency.GBP.equals(base)) {
        if (Currency.USD.equals(currentCurrency)) {
          return USER_HAVE_IN_USD / data.getUsd();
        } else if (Currency.GBP.equals(currentCurrency)) {
          return USER_HAVE_IN_USD * data.getGbp() / data.getUsd();
        } else if (Currency.EUR.equals(currentCurrency)) {
          return USER_HAVE_IN_USD * data.getEur() / data.getUsd();
        }
      } else if (Currency.USD.equals(base)) {
        if (Currency.USD.equals(currentCurrency)) {
          return USER_HAVE_IN_USD;
        } else if (Currency.GBP.equals(currentCurrency)) {
          return USER_HAVE_IN_USD * data.getGbp();
        } else if (Currency.EUR.equals(currentCurrency)) {
          return USER_HAVE_IN_USD * data.getEur();
        }
      }
    } catch (Exception ex) {
      return 0.0d;
    }

    return 0.0d;
  }

  /**
   * usd base unit for computations
   */
  private static double rateOfExchangeUsd(CurrencyData data) {
    String base = data.getBase();
    String currency = data.getCurrency();
    if (Currency.USD.equals(currency)) {
      return 1.0d;
    } else if (Currency.GBP.equals(currency)) {
      if (Currency.USD.equals(base)) {
        return 1 / data.getGbp();
      } else if (Currency.EUR.equals(base)) {
        return data.getUsd() / data.getGbp();
      } else if (Currency.GBP.equals(base)) {
        return data.getUsd();
      }
    } else if (Currency.EUR.equals(currency)) {
      if (Currency.USD.equals(base)) {
        return 1 / data.getEur();
      } else if (Currency.EUR.equals(base)) {
        return data.getUsd();
      } else if (Currency.GBP.equals(base)) {
        return data.getUsd() / data.getEur();
      }
    }

    return 1.0d;
  }

  /**
   * eur base unit for computations
   */
  private static double rateOfExchangeEur(CurrencyData data) {
    String base = data.getBase();
    String currency = data.getCurrency();
    if (Currency.USD.equals(currency)) {
      if (Currency.USD.equals(base)) {
        return data.getEur();
      } else if (Currency.EUR.equals(base)) {
        return 1 / data.getUsd();
      } else if (Currency.GBP.equals(base)) {
        return data.getEur() / data.getUsd();
      }
    } else if (Currency.EUR.equals(currency)) {
      return 1.0d;
    } else if (Currency.GBP.equals(currency)) {
      if (Currency.USD.equals(base)) {
        return data.getEur() / data.getGbp();
      } else if (Currency.EUR.equals(base)) {
        return 1 / data.getGbp();
      } else if (Currency.GBP.equals(base)) {
        return data.getGbp();
      }
    }
    return 1.0d;
  }

  /**
   * gbp base unit for computations
   */
  private static double rateOfExchangeGbp(CurrencyData data) {
    String base = data.getBase();
    String currency = data.getCurrency();
    if (Currency.USD.equals(currency)) {
      if (Currency.USD.equals(base)) {
        return data.getGbp();
      } else if (Currency.EUR.equals(base)) {
        return data.getGbp() / data.getUsd();
      } else if (Currency.GBP.equals(base)) {
        return 1 / data.getUsd();
      }
    } else if (Currency.EUR.equals(currency)) {
      if (Currency.USD.equals(base)) {
        return data.getGbp() / data.getEur();
      } else if (Currency.EUR.equals(base)) {
        return data.getGbp();
      } else if (Currency.GBP.equals(base)) {
        return 1 / data.getEur();
      }
    } else if (Currency.GBP.equals(currency)) {
      return 1.0d;
    }

    return 1.0d;
  }

  public static double getRateOfExchange(CurrencyData data, String currency) {
    if (Currency.USD.equals(currency)) {
      return rateOfExchangeUsd(data);
    } else if (Currency.EUR.equals(currency)) {
      return rateOfExchangeEur(data);
    } else if (Currency.GBP.equals(currency)) {
      return rateOfExchangeGbp(data);
    }

    return 0.0d;
  }

  public static String currencySymbol(Context context, String currency) {
    if (Currency.USD.equals(currency)) {
      return context.getString(R.string.dollar);
    } else if (Currency.EUR.equals(currency)) {
      return context.getString(R.string.euro);
    } else if (Currency.GBP.equals(currency)) {
      return context.getString(R.string.pound);
    }

    return "";
  }

  /**
   * converter
   */
  public static double value(CurrencyData data, String currency, double value) {
    if (Currency.USD.equals(currency)) {
      return value * 1 / rateOfExchangeUsd(data);
    } else if (Currency.EUR.equals(currency)) {
      return value * 1 /rateOfExchangeEur(data);
    } else if (Currency.GBP.equals(currency)) {
      return value * 1 /rateOfExchangeGbp(data);
    }

    return value;
  }

  public static String formRateOfExchangeString(Context context, CurrencyData data,
      String currency) {
    return currencySymbol(context, data.getCurrency())
        + " 1 = "
        + currencySymbol(context, currency)
        + " "
        + fourDigitsFormat(getRateOfExchange(data, currency));
  }

  public static String invertOfExchangeString(Context context, CurrencyData data, String currency) {
    return currencySymbol(context, currency)
        + " 1 = "
        + currencySymbol(context, data.getCurrency())
        + " "
        + fourDigitsFormat(1 / getRateOfExchange(data, currency));
  }
}
