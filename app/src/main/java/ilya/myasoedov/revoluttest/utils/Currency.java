package ilya.myasoedov.revoluttest.utils;


public interface Currency {
  String USD = "USD";
  String GBP = "GBP";
  String EUR = "EUR";
}
