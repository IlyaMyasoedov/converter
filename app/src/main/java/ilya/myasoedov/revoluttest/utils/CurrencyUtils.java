package ilya.myasoedov.revoluttest.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.TextView;
import ilya.myasoedov.revoluttest.R;
import ilya.myasoedov.revoluttest.data.model.CurrencyData;
import java.util.Locale;

import static ilya.myasoedov.revoluttest.navigation.RequestKey.BASE_CURRENCY;
import static ilya.myasoedov.revoluttest.navigation.RequestKey.CURRENCY_DATA;
import static ilya.myasoedov.revoluttest.navigation.RequestKey.IS_BOTTOM;
import static ilya.myasoedov.revoluttest.utils.ConverterUtils.currencySymbol;

public class CurrencyUtils {
  private CurrencyUtils() {
    throw new AssertionError();
  }

  public static void setupYouHave(TextView textView, double youHave, String currency) {
    Context context = textView.getContext();
    String youHaveString = context.getString(R.string.you_have_text)
        + " "
        + currencySymbol(context, currency)
        + " "
        + defaultFormat(youHave);
    textView.setText(youHaveString);
  }

  public static void addToBundle(Fragment fragment, CurrencyData data, String baseCurrency,
      boolean isBottom) {
    Bundle bundle = new Bundle();
    bundle.putParcelable(CURRENCY_DATA, data);
    bundle.putBoolean(IS_BOTTOM, isBottom);
    bundle.putString(BASE_CURRENCY, baseCurrency);

    fragment.setArguments(bundle);
  }

  public static String defaultFormat(double value) {
    return String.format(Locale.US, "%.2f", value);
  }

  public static String fourDigitsFormat(double value){
    return String.format(Locale.US, "%.4f", value);
  }
}
