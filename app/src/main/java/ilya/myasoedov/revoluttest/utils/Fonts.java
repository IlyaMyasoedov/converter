package ilya.myasoedov.revoluttest.utils;

import android.content.Context;
import android.graphics.Typeface;

public enum Fonts {
  ROBOTO_BOLD("Roboto-Bold.ttf"),
  ROBOTO_LIGHT("Roboto-Light.ttf"),
  ROBOTO_MEDIUM("Roboto-Medium.ttf"),
  ROBOTO_REGULAR("Roboto-Regular.ttf"),
  ROBOTO_THIN("Roboto-Thin.ttf"),
  MENLO_REGULAR("Menlo-Regular.ttf"),
  OPEN_GOST_TYPE_B("OpenGostTypeB-Regular.ttf");

  public static Typeface robotoBold;
  public static Typeface robotoLight;
  public static Typeface robotoMedium;
  public static Typeface robotoRegular;
  public static Typeface robotoThin;
  public static Typeface ruble;
  public static Typeface menloRegular;
  public static Typeface OpenGostTypeB;

  private static final String FOLDER_FONTS = "fonts/";

  private String fontName;

  Fonts(String fontName) {
    this.fontName = FOLDER_FONTS + fontName;
  }

  public Typeface asTypeface(Context context) {
    return Typeface.createFromAsset(context.getAssets(), fontName);
  }

  public static void loadAllFonts(Context context) {
    robotoBold = ROBOTO_BOLD.asTypeface(context);
    robotoLight = ROBOTO_LIGHT.asTypeface(context);
    robotoMedium = ROBOTO_MEDIUM.asTypeface(context);
    robotoRegular = ROBOTO_REGULAR.asTypeface(context);
    robotoThin = ROBOTO_THIN.asTypeface(context);
    menloRegular = MENLO_REGULAR.asTypeface(context);
    OpenGostTypeB = OPEN_GOST_TYPE_B.asTypeface(context);
  }
}
