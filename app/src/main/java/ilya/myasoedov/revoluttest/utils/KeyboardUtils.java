package ilya.myasoedov.revoluttest.utils;

import android.app.Activity;
import android.content.Context;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtils {
  public static void hideKeyboard(Activity activity) {
    try {
      activity.getWindow()
          .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
      if ((activity.getCurrentFocus() != null) && (activity.getCurrentFocus().getWindowToken()
          != null)) {
        ((InputMethodManager) activity.getSystemService(
            Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
            activity.getCurrentFocus().getWindowToken(), 0);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void showKeyboard(Context context) {
    if (context != null) {
      InputMethodManager imm =
          (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
  }
}
