package ilya.myasoedov.revoluttest.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

public class PopupUtils {

  private PopupUtils(){
    throw new AssertionError();
  }

  public static void showMessage(View view, String message) {
    Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
    snackbar.show();
  }
}
