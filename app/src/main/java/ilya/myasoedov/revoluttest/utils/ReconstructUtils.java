package ilya.myasoedov.revoluttest.utils;

import ilya.myasoedov.revoluttest.data.db.entity.LatestLocal;
import ilya.myasoedov.revoluttest.data.model.CurrencyData;
import ilya.myasoedov.revoluttest.data.model.Rates;
import ilya.myasoedov.revoluttest.data.net.LatestResponse;
import java.util.ArrayList;
import java.util.List;

import static ilya.myasoedov.revoluttest.utils.AppsUtils.USER_HAVE_IN_USD;

public class ReconstructUtils {
  private ReconstructUtils() {
    throw new AssertionError();
  }

  public static LatestLocal toLatestLocal(LatestResponse response) {
    LatestLocal local = new LatestLocal();
    if (response == null) {
      return local;
    }
    local.setBase(response.getBase());
    local.setDate(response.getDate());
    Rates rates = response.getRates();
    if (rates != null) {
      local.setEur(rates.getEur());
      local.setGbp(rates.getGbp());
      local.setUsd(rates.getUsd());
    }
    return local;
  }

  public static List<CurrencyData> toCurrencyData(LatestLocal latestLocal) {
    List<CurrencyData> datas = new ArrayList<>();

    if (latestLocal == null) {
      return datas;
    }

    CurrencyData usdData = new CurrencyData();
    usdData.setBase(latestLocal.getBase());
    usdData.setCurrency(Currency.USD);
    usdData.setUsd(latestLocal.getUsd());
    usdData.setEur(latestLocal.getEur());
    usdData.setGbp(latestLocal.getGbp());

    CurrencyData gbpData = new CurrencyData();
    gbpData.setBase(latestLocal.getBase());
    gbpData.setCurrency(Currency.GBP);
    gbpData.setUsd(latestLocal.getUsd());
    gbpData.setEur(latestLocal.getEur());
    gbpData.setGbp(latestLocal.getGbp());

    CurrencyData eurData = new CurrencyData();
    eurData.setBase(latestLocal.getBase());
    eurData.setCurrency(Currency.EUR);
    eurData.setUsd(latestLocal.getUsd());
    eurData.setEur(latestLocal.getEur());
    eurData.setGbp(latestLocal.getGbp());

    datas.add(usdData);
    datas.add(gbpData);
    datas.add(eurData);

    return datas;
  }

  /*public static List<CurrencyData> mockyData() {
    List<CurrencyData> datas = new ArrayList<>();

    CurrencyData usdData = new CurrencyData();
    usdData.setCurrency("USD");
    usdData.setRateOfExchange(232.3);
    usdData.setYouHave(USER_HAVE_IN_USD);

    CurrencyData gbpData = new CurrencyData();
    gbpData.setCurrency("GBP");
    gbpData.setRateOfExchange(232.3);
    gbpData.setYouHave(USER_HAVE_IN_USD);

    CurrencyData eurData = new CurrencyData();
    eurData.setCurrency("EUR");
    eurData.setRateOfExchange(232.3);
    eurData.setYouHave(USER_HAVE_IN_USD);

    datas.add(usdData);
    datas.add(gbpData);
    datas.add(eurData);

    return datas;
  }*/
}
