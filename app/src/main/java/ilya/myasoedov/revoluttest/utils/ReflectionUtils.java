package ilya.myasoedov.revoluttest.utils;

public class ReflectionUtils {
  public static boolean cast(Class<?> clazz, Object object) {
    return !(object == null || clazz == null) && cast(clazz, object.getClass());
  }

  public static boolean cast(Class<?> clazz1, Class<?> clazz2) {
    return !(clazz1 == null || clazz2 == null) && clazz1.isAssignableFrom(clazz2);
  }
}
