package ilya.myasoedov.revoluttest.utils;

public class StringUtils {
  public static String removeTrailingZeros(String text) {
    text = text.indexOf(".") < 0 ? text : text.replaceAll("0*$", "").replaceAll("\\.$", "");
    return text;
  }
}
