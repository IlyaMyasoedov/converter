package ilya.myasoedov.revoluttest.utils;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import java.lang.reflect.Field;

public class ToolbarUtils {
  public static void setTitleFont(Typeface typeface, @NonNull Toolbar toolbar) {
    TextView titleTextView = null;
    try {
      Field f = toolbar.getClass().getDeclaredField("mTitleTextView");
      f.setAccessible(true);
      titleTextView = (TextView) f.get(toolbar);
      if (titleTextView != null) {
        titleTextView.setTypeface(typeface);
      }
    } catch (NoSuchFieldException e) {
    } catch (IllegalAccessException e) {
    }
  }

  public static void setSubtitleFont(Typeface typeface, @NonNull Toolbar toolbar) {
    TextView subtitleTextView = null;
    try {
      Field f = toolbar.getClass().getDeclaredField("mSubtitleTextView");
      f.setAccessible(true);
      subtitleTextView = (TextView) f.get(toolbar);
      if (subtitleTextView != null) {
        subtitleTextView.setTypeface(typeface);
      }
    } catch (NoSuchFieldException e) {
    } catch (IllegalAccessException e) {
    }
  }
}
