package ilya.myasoedov.revoluttest.utils;

import android.text.InputFilter;
import android.view.View;
import android.widget.TextView;

import static android.R.attr.maxLength;
import static android.view.View.GONE;

public class UiUtils {

  private UiUtils() {
    throw new AssertionError();
  }

  public static boolean isViewOverlapping(View firstView, View secondView) {
    if (firstView.getVisibility() == GONE || secondView.getVisibility() == GONE) {
      return false;
    }

    int[] firstPosition = new int[2];
    int[] secondPosition = new int[2];

    firstView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
    firstView.getLocationOnScreen(firstPosition);
    secondView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
    secondView.getLocationOnScreen(secondPosition);

    return firstPosition[0] < secondPosition[0] + getWidthWithPadding(secondView)
        && firstPosition[0] + getWidthWithPadding(firstView) > secondPosition[0]
        && firstPosition[1] < secondPosition[1] + getHeightWithPadding(secondView)
        && firstPosition[1] + getHeightWithPadding(firstView) > secondPosition[1];
  }

  public static int getWidthWithPadding(View view){
    return view.getMeasuredWidth() + view.getPaddingStart() + view.getPaddingEnd();
  }

  public static int getHeightWithPadding(View view){
    return view.getMeasuredHeight() + view.getPaddingTop() + view.getPaddingBottom();
  }

  public static void setMaxLength(TextView textView, int maxLength){
    InputFilter[] fArray = new InputFilter[1];
    fArray[0] = new InputFilter.LengthFilter(maxLength);
    textView.setFilters(fArray);
  }
}
